﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TigerIntegration.Entity;

namespace TigerIntegration.DataOperation
{
    public class SysParameterDO
    {
        private SqlConnection con;
        string firmNrStr;
        private void connection()
        {
            string conStr = ConfigurationManager.AppSettings["connectionString"];
            con = new SqlConnection(conStr);
        }
        public void Update(SysParameterVM _sysParameter)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.CommandText = "NTI_" + firmNrStr + "_Upd_Parameters";
            
            cmd.Parameters.Add("@LogicalRef", SqlDbType.Int).Value = _sysParameter.logicalRef;
            cmd.Parameters.Add("@ApprovalDate", SqlDbType.DateTime).Value = _sysParameter.approvalDate;
            
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public SysParameterVM Select()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_" + firmNrStr + "_Sel_Parameters";
           
            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                SysParameterVM sysParameterVM = new SysParameterVM();
                
                while (reader.Read())
                {
                    sysParameterVM.logicalRef = (int)reader["LogicalRef"];
                    sysParameterVM.approvalDate = (DateTime)reader["ApprovalDate"];
                }

                return sysParameterVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public SysParameterDO()
        {
            connection();
            firmNrStr = ConfigurationManager.AppSettings["firmNr"];
            firmNrStr = firmNrStr.PadLeft(3, '0');
        }
    }
}
