﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.Entity;


namespace TigerIntegration.DataOperation
{
    public class CLCardDO
    {
        private SqlConnection con;
        string firmNrStr;
        string periodNrStr;
        private void connection()
        {
            string conStr = ConfigurationManager.AppSettings["connectionString"];
            con = new SqlConnection(conStr);
        }
        public CLCardDO()
        {
            connection();
            firmNrStr = ConfigurationManager.AppSettings["firmNr"];
            firmNrStr = firmNrStr.PadLeft(3, '0');
            periodNrStr = ConfigurationManager.AppSettings["periodNr"];
            periodNrStr = periodNrStr.PadLeft(2, '0');
            periodNrStr = "_" + periodNrStr;
        }

        public LogoClCard SelectByCustTypeVKN(short _custType, string _custTCKN)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_" + firmNrStr + "_Sel_ClcardByCustTypeVKN";

            cmd.Parameters.Add("@CustType", SqlDbType.SmallInt).Value = _custType;
            cmd.Parameters.Add("@VKN", SqlDbType.VarChar).Value = _custTCKN;


            cmd.Connection = con;
            LogoClCard logoClCard = new LogoClCard();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
               

                while (reader.Read())
                {
                    logoClCard.Code = (string)reader["CODE"];
                    logoClCard.AcceptEInvoice = (short)reader["ACCEPTEINV"];
                    logoClCard.EMail = (string)reader["EMAIL"];
                    logoClCard.EInvoiceTyp = (short)reader["EINVOICETYP"];
                    logoClCard.IsPersComp = (short)reader["ISPERSCOMP"];
                    logoClCard.Address1 = (string)reader["ADDR1"];
                    logoClCard.Address2 = (string)reader["ADDR2"];
                    logoClCard.ProfileId = (short)reader["PROFILEID"];
                    logoClCard.IsPerCurr = (short)reader["ISPERCURR"];
                }

                return logoClCard;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public LogoClCard SelectByCustCode( string _code)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_" + firmNrStr + "_Sel_ClcardByCode";

            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = _code;
            

            cmd.Connection = con;
            LogoClCard logoClCard = new LogoClCard();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    logoClCard.Code = (string)reader["CODE"];
                    logoClCard.AcceptEInvoice = (short)reader["ACCEPTEINV"];
                    logoClCard.EMail = (string)reader["EMAIL"];
                    logoClCard.EInvoiceTyp = (short)reader["EINVOICETYP"];
                    logoClCard.IsPersComp = (short)reader["ISPERSCOMP"];
                    logoClCard.Address1 = (string)reader["ADDR1"];
                    logoClCard.Address2 = (string)reader["ADDR2"];
                    logoClCard.ProfileId = (short)reader["PROFILEID"];
                    logoClCard.IsPerCurr = (short)reader["ISPERCURR"];
                }

                return logoClCard;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}
