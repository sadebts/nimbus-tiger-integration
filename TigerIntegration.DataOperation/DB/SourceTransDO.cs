﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TigerIntegration.Entity;

namespace TigerIntegration.DataOperation
{
    public class SourceTransDO
    {
        private SqlConnection con;
        string firmNrStr;
        string periodNrStr;
        private void connection()
        {
            string conStr = ConfigurationManager.AppSettings["connectionString"];
            con = new SqlConnection(conStr);
        }
        static DataTable BnfLineTableVariable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BranchId", typeof(int));
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("OrgLogoId", typeof(string));
            dt.Columns.Add("CUSTOMDOCNR", typeof(string));
            dt.Columns.Add("TranAccType", typeof(int));
            dt.Columns.Add("OriginalSlipDate", typeof(DateTime));
            dt.Columns.Add("TranSerialNumber", typeof(int));
            return dt;
        }

        static DataTable ExpenseVMTableVariable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("LineRefNumber", typeof(int));
            return dt;
        }
        static DataTable IntList()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Reference", typeof(int));
            return dt;
        }

        public List<SourceTransVM> SelectBySlipDateBranchNrSlipRef(List<SourceTransVM> _incomingList, bool _isOld, bool _isClfLine)
        {
            List<SourceTransVM> orderedList = new List<SourceTransVM>();
            orderedList = _incomingList.OrderBy(x => x.SlipBranchNr)
                                       .ThenBy(x => x.SlipDate)
                                       .ThenBy(x => x.SlipRefNumber)
                                       .ToList();
            
            DataTable bnfLineTableVariable = BnfLineTableVariable();

            foreach (var item in orderedList)
            {
                if (_isOld)
                    bnfLineTableVariable.Rows.Add(item.OldSlipBranchNr, item.OldSlipDate, item.OldSlipRefNumber.ToString(),"",item.TranAccType,item.OriginalSlipDate);
                else
                    bnfLineTableVariable.Rows.Add(item.SlipBranchNr, item.SlipDate.Date, item.SlipRefNumber.ToString(), item.TranSerialNumber.ToString(),item.TranAccType, item.OriginalSlipDate);
            }

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if(_isClfLine)
            {
                cmd.CommandText = "NTI_"+ firmNrStr + periodNrStr+ "_Sel_ClfLineByBranchDateOrgLogoId";
            }
               
            else
            {
                cmd.CommandText = "NTI_" + firmNrStr + periodNrStr+ "_Sel_BnfLineByBranchDateOrgLogoId";
            }
                

            cmd.Parameters.Add("@Table", SqlDbType.Structured).Value = bnfLineTableVariable;

            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                
                List<SourceTransVM> sourceTranList = new List<SourceTransVM>();

                while (reader.Read())
                {
                    SourceTransVM sourceTransVM = new SourceTransVM();
                    sourceTransVM.TranAccountCode = (string)reader["ARPCODE"];
                    sourceTransVM.BankAccountCode = (string)reader["BANKACCOUNTCODE"];
                    sourceTransVM.TranTotal = (double)reader["AMOUNT"];
                    sourceTransVM.TranExp = (string)reader["LINEEXP"];
                    sourceTransVM.TranDocumentNo = (string)reader["DOCNR"];
                    sourceTransVM.SlipBranchNr = Convert.ToInt16(reader["BRANCHNR"]);
                    sourceTransVM.SlipDate = (DateTime)reader["SLIPDATE"];
                    sourceTransVM.SlipRefNumber = Convert.ToInt32(reader["SLIPREFNUMBER"]);
                    sourceTransVM.TigerTranLogicalRef = (int)reader["LOGICALREF"];
                    
                    sourceTransVM.SlipType = Convert.ToInt32(reader["SlipType"]);
                    sourceTransVM.ficheType = _isClfLine ? 2 : 1;
                    sourceTransVM.ficheNo   = (string)reader["ficheNo"];
                    sourceTransVM.TranAccType = Convert.ToInt16(reader["TranAccType"]);
                    
                    sourceTransVM.TranSerialNumber = Convert.ToInt32(reader["TranSerialNumber"]);
                    sourceTransVM.PayTransRef = 0;

                    if (_isClfLine)
                    {
                        sourceTransVM.OriginalSlipDate = (DateTime)reader["OriginalSlipDate"];
                        /*
                        sourceTransVM.OriginalSlipDate = orderedList.ElementAt(i).OriginalSlipDate;
                        sourceTransVM.TranSerialNumber = orderedList.ElementAt(i).TranSerialNumber;
                        */
                    }
                    else
                    {
                        sourceTransVM.OriginalSlipDate = (DateTime)reader["OriginalSlipDate"];
                        sourceTransVM.PayTransRef      = Convert.ToInt32(reader["PAYTRANSREF"]);
                    }
                        

                    sourceTranList.Add(sourceTransVM);
                }

                return sourceTranList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<SourceTransVM> Select4BankAcc(List<SourceTransVM> _incomingList)
        {

            DataTable bnfLineTableVariable = BnfLineTableVariable();

            foreach (var item in _incomingList)
            {
                bnfLineTableVariable.Rows.Add(item.SlipBranchNr, item.SlipDate, item.SlipRefNumber.ToString(),"");
            }

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "NTI_" + firmNrStr + periodNrStr+ "_Sel_BnfLine4BankAcc";

            cmd.Parameters.Add("@Table", SqlDbType.Structured).Value = bnfLineTableVariable;

            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<SourceTransVM> sourceTranList = new List<SourceTransVM>();

                while (reader.Read())
                {
                    SourceTransVM sourceTransVM = new SourceTransVM();
                    sourceTransVM.BankAccountCode = (string)reader["BANKACCOUNTCODE"];
                    sourceTransVM.SlipBranchNr = Convert.ToInt16(reader["BRANCHNR"]);
                    sourceTransVM.SlipDate = (DateTime)reader["SLIPDATE"];
                    sourceTransVM.SlipRefNumber = Convert.ToInt32(reader["SLIPREFNUMBER"]);
                    sourceTransVM.TigerTranLogicalRef = (int)reader["LOGICALREF"];
                    sourceTransVM.TranSerialNumber = _incomingList.First(x => x.SlipBranchNr == sourceTransVM.SlipBranchNr
                                                                            && x.SlipDate == sourceTransVM.SlipDate
                                                                            && x.SlipRefNumber == sourceTransVM.SlipRefNumber
                                                                         ).TranSerialNumber;
                    sourceTranList.Add(sourceTransVM);
                }

                return sourceTranList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<IncomingRefPayTransRef> SelectPaytransByInvoiceRef(SourceTransVM _incomingSourceTransVM)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            
            DataTable intList = IntList();
            intList.Rows.Add(_incomingSourceTransVM.InvoiceRef);
            


            cmd.CommandText = "NTI_" + firmNrStr + periodNrStr + "_Sel_PayTransByInvoiceRef";
            
            cmd.Parameters.Add("@Table", SqlDbType.Structured).Value = intList;
            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<IncomingRefPayTransRef> payTransRefList = new List<IncomingRefPayTransRef>();

                while (reader.Read())
                {
                    
                    IncomingRefPayTransRef invoiceRefPayTransRef = new IncomingRefPayTransRef();
                    invoiceRefPayTransRef.incomingRef = Convert.ToInt32(reader["IncomingRef"]);
                    invoiceRefPayTransRef.payTransRef = Convert.ToInt32(reader["PAYTRANSREF"]);
                    payTransRefList.Add(invoiceRefPayTransRef);
                }

                return payTransRefList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<ExpenseVM> SelectInsertedExpenseList(List<ExpenseVM> _incomingList)
        {

            DataTable expenseVMTableVariable = ExpenseVMTableVariable();
            int lnRefNumber;
            foreach (var item in _incomingList)
            {
                expenseVMTableVariable.Rows.Add(item.LineRefNumber);
            }

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "NTI_" + firmNrStr + periodNrStr + "_Sel_ExpenseListByLineRef";

            cmd.Parameters.Add("@Table", SqlDbType.Structured).Value = expenseVMTableVariable;

            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<ExpenseVM> expenseVMList = new List<ExpenseVM>();

                while (reader.Read())
                {
                    
                    lnRefNumber = (int)reader["LineRefNumber"];

                    foreach (var line in _incomingList.Where(i => i.LineRefNumber == lnRefNumber))
                    {
                        expenseVMList.Add(line);
                    }
                }

                return expenseVMList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public SourceTransDO()
        {
            connection();
            firmNrStr = ConfigurationManager.AppSettings["firmNr"];
            firmNrStr = firmNrStr.PadLeft(3, '0');
            periodNrStr = ConfigurationManager.AppSettings["periodNr"];
            periodNrStr = periodNrStr.PadLeft(2, '0');
            periodNrStr = "_" + periodNrStr;
        }
    }
}
