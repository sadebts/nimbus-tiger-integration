﻿using TigerIntegration.Data;
using TigerIntegration.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.DataOperation
{
    public class SysUserDO
    {
        private SqlConnection con;
        //To Handle connection related activities
        private void connection()
        {
            con = new SqlConnection(DBConfig.connectionString); 
        }
        public int Insert(SysUserVM _sysUser)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ins_SysUser";
            cmd.Parameters.Add("@SysCustomerId", SqlDbType.Int).Value = _sysUser.sysCustomerId;
            cmd.Parameters.Add("@EMail", SqlDbType.VarChar).Value = _sysUser.eMail;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = _sysUser.password;
            cmd.Parameters.Add("@Status", SqlDbType.TinyInt).Value = _sysUser.status;
            var returnParameter = cmd.Parameters.Add("@SysUserId", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.Output;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                var result = (int)returnParameter.Value;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public void Update(SysUserVM _sysUser)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "upd_SysUser";
            cmd.Parameters.Add("@SysUserId", SqlDbType.Int).Value = _sysUser.sysUserId;
            cmd.Parameters.Add("@SysCustomerId", SqlDbType.Int).Value = _sysUser.sysCustomerId;
            cmd.Parameters.Add("@EMail", SqlDbType.VarChar).Value = _sysUser.eMail;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = _sysUser.password;
            cmd.Parameters.Add("@Status", SqlDbType.TinyInt).Value = _sysUser.status;

            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        /*
           public SysUserDM SelectById(int sysUserId)
          {
              SqlCommand cmd = new SqlCommand();
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.CommandText = "sel_SysUser";
              cmd.Parameters.Add("@SysUserId", SqlDbType.Int).Value = sysUserId;

              cmd.Connection = con;
              try
              {
                  con.Open();
                  SqlDataReader reader = cmd.ExecuteReader();
                  SysUserDM _sysUserDM = new SysUserDM();
                  while (reader.Read())
                  {
                      _sysUserDM.SysUserId = (int)reader["SysUserId"];
                      _sysUserDM.SysCustomerId = (int)reader["SysCustomerId"];
                      _sysUserDM.EMail = (string)reader["EMail"];
                      _sysUserDM.Password = (string)reader["Password"];
                      _sysUserDM.Status = (byte)reader["Status"];
                  }

                  return _sysUserDM;

              }
              catch (Exception ex)
              {
                  throw ex;
              }
              finally
              {
                  con.Close();
                  con.Dispose();
              }
          }
          */
        public List<SysUserVM> SelectBySysCustomerId(int _sysCustomerId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sel_SysUserBySysCustomerId";
            cmd.Parameters.Add("@SysCustomerId", SqlDbType.Int).Value = _sysCustomerId;

            cmd.Connection = con;
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<SysUserVM> _sysUserVMList = new List<SysUserVM>();
                while (reader.Read())
                {
                    SysUserVM _sysUserVM = new SysUserVM();
                    _sysUserVM.sysUserId = (int)reader["SysUserId"];
                    _sysUserVM.sysCustomerId = (int)reader["SysCustomerId"];
                    _sysUserVM.eMail = (string)reader["EMail"];
                    _sysUserVM.password = (string)reader["Password"];
                    _sysUserVM.status = (byte)reader["Status"];

                    _sysUserVMList.Add(_sysUserVM);
                }

                return _sysUserVMList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        //public SysUserBS(SqlConnection _connection = null)
        //{
        //    if (_connection == null)
        //        connection();
        //    else
        //        con = _connection;
        //}

        public SysUserDO()
        {
                connection();
        }

        public SysUserContextVM Login(string _eMail, string _password, int _sysCustomerId, bool _withProfile = false)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sel_Login";
            cmd.Parameters.Add("@SysCustomerId", SqlDbType.Int).Value = _sysCustomerId; 
            cmd.Parameters.Add("@EMail", SqlDbType.VarChar).Value = _eMail;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = _password;
//            var returnParameter = cmd.Parameters.Add("@Result", SqlDbType.SmallInt);
//            returnParameter.Direction = ParameterDirection.Output;
            cmd.Connection = con;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

 //               cmd.ExecuteNonQuery();
                SysUserContextVM sysUserContextVM = new SysUserContextVM();
                SysUserVM sysUserVM = new SysUserVM();
                while (reader.Read())
                {
                    sysUserVM.sysUserId = (int)reader["SysUserId"];
                    sysUserVM.sysCustomerId = (int)reader["SysCustomerId"];
                    sysUserVM.eMail = (string)reader["EMail"];
                    sysUserVM.password = (string)reader["Password"];
                    sysUserVM.status = (byte)reader["Status"];

                    if (_withProfile && sysUserVM != null)
                    {
                        sysUserContextVM.sysUserVM = sysUserVM;
                        SysUserProfileDO sysUserProfileBS = new SysUserProfileDO();
                        List<SysUserProfileVM> sysUserProfileVMList = new List<SysUserProfileVM>();
                        sysUserProfileVMList = sysUserProfileBS.SelectBySysUserId(sysUserVM.sysUserId);

                        foreach(SysUserProfileVM item in sysUserProfileVMList)
                        {
                            sysUserContextVM.profileList.Add(item.profileId);
                        }
                    }
                }

                return sysUserContextVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}
