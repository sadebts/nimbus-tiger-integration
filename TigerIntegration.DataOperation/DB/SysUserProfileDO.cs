﻿using TigerIntegration.Data;
using TigerIntegration.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.DataOperation
{
    public class SysUserProfileDO
    {
        private SqlConnection con;
        //To Handle connection related activities
        private void connection()
        {
            con = new SqlConnection(DBConfig.connectionString);
        }

        public SysUserProfileDO()
        {
            connection();
        }
        public List<SysUserProfileVM> SelectBySysUserId(int _sysUserId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sel_SysUserProfileBySysUserId";
            cmd.Parameters.Add("@SysUserId", SqlDbType.Int).Value = _sysUserId;

            cmd.Connection = con;
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<SysUserProfileVM> sysUserProfileVMList = new List<SysUserProfileVM>();
                while (reader.Read())
                {
                    SysUserProfileVM sysUserProfileVM = new SysUserProfileVM();
                    sysUserProfileVM.sysUserId = (int)reader["SysUserId"];
                    sysUserProfileVM.sysCustomerId = (int)reader["SysCustomerId"];
                    sysUserProfileVM.profileId = (int)reader["ProfileId"];
                    sysUserProfileVMList.Add(sysUserProfileVM);
                }

                return sysUserProfileVMList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}
