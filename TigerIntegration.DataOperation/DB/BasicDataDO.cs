﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.Entity;

namespace TigerIntegration.DataOperation
{
    public class BasicDataDO
    {
        private SqlConnection con;
        string firmNrStr;
        string periodNrStr;
        private void connection()
        {
            string conStr = ConfigurationManager.AppSettings["connectionString"];
            con = new SqlConnection(conStr);
        }
        public BasicDataDO()
        {
            connection();
            firmNrStr = ConfigurationManager.AppSettings["firmNr"];
            firmNrStr = firmNrStr.PadLeft(3, '0');
            periodNrStr = ConfigurationManager.AppSettings["periodNr"];
            periodNrStr = periodNrStr.PadLeft(2, '0');
            periodNrStr = "_" + periodNrStr;
        }

        public List<CountryVM> SelectCountryList()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_Sel_CountryList";

            cmd.Connection = con;
            List<CountryVM> resultList = new List<CountryVM>();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    CountryVM countryVM = new CountryVM();

                    countryVM.LogicalRef = (int)reader["LOGICALREF"];
                    countryVM.CountryNr = (int)reader["COUNTRYNR"];
                    countryVM.Code = (string)reader["CODE"];
                    countryVM.Name = (string)reader["NAME"];
                    resultList.Add(countryVM);
                }

                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<CityVM> SelectCityList()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_Sel_CityList";

            cmd.Connection = con;
            List<CityVM> resultList = new List<CityVM>();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    CityVM cityVM = new CityVM();

                    cityVM.LogicalRef = (int)reader["LOGICALREF"];
                    cityVM.CountryNr = (int)reader["COUNTRY"];
                    cityVM.Code = (string)reader["CODE"];
                    cityVM.Name = (string)reader["NAME"];
                    resultList.Add(cityVM);
                }

                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<TownVM> SelectTownList()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_Sel_TownList";

            cmd.Connection = con;
            List<TownVM> resultList = new List<TownVM>();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    TownVM townVM = new TownVM();

                    townVM.LogicalRef = (int)reader["LOGICALREF"];
                    townVM.CityRef = (int)reader["CTYREF"];
                    townVM.Code = (string)reader["CODE"];
                    townVM.Name = (string)reader["NAME"];
                    resultList.Add(townVM);
                }

                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public SrvCard SelectSrvCardByCode(string _code)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "NTI_" + firmNrStr + "_Sel_SrvCardByCode";

            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = _code;
            

            cmd.Connection = con;
            SrvCard srvCard = new SrvCard();
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    srvCard.LogicalRef = (int)reader["LOGICALREF"];

                    srvCard.Code = (string)reader["CODE"];
                    srvCard.VAT = (double)reader["VAT"];
                    
                }

                return srvCard;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}
