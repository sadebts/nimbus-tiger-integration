﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.DataOperation.WebService
{
    public class SourceTrans
    {
        public int SerialNumber { get; set; }
        public int TranBranchCode { get; set; }
        public DateTime TranDate { get; set; }
        public int? TranRefNum { get; set; }
        public string TranCode  { get; set; }

        public int? CancellationTranBranch { get; set; }
        public DateTime? CancellationTranDate { get; set; }
       // public string CancellationTranDate { get; set; }
        public int? CancellationTranRefNum { get; set; }
        public string DocumentNumber { get; set; }
        public string TranAccountCode { get; set; }
        public double? Amount { get; set; }
        public string Currency { get; set; }
        public string DebitOrCredit { get; set; }
        public string Description { get; set; }
        public string CancellationFlag { get; set; }

       // public int? TranAccType { get; set; }
        public int TranLineType { get; set; }
        public string CustType { get; set; }
        public string CustTaxOffice { get; set; }
       // public short? CustSendType { get; set; }
        public string CustCountry { get; set; }
        public string CustCity { get; set; }
        public string CustTown { get; set; }
        public string CustTaxNr { get; set; }
        public string CustName { get; set; }
        public string CustSurname { get; set; }
        public string CustTCKN { get; set; }
        public string CustTitle { get; set; }
        public string CustAddress1 { get; set; }
        public string CustAddress2 { get; set; }
        public string CustEMail { get; set; }
        public string ServiceCode { get; set; }
        ////public int? CancellationFlag { get; set; }
        //public int? TranSequence { get; set; }
        //public int AccBranch { get; set; }
        //public int AccNumber { get; set; }
        //public int AccSuffix { get; set; }
        //public string GLNumber { get; set; }
        //public int? DocumentType { get; set; }
        //public DateTime? DocumentDate { get; set; }

        //public int SlipType { get; set; }
        //public int TranAccType { get; set; }
        //public int TranSendType { get; set; }
        //public string TranDocumentNo { get; set; }
        //public string BankAccountCode { get; set; }
        //public string TranAccountCode { get; set; }
        //public double TranTotal { get; set; }
        //public string TranExp { get; set; }

        //public DateTime OldSlipDate { get; set; }
        //public int OldSlipRefNumber { get; set; }
        //public int OldSlipBranchNr { get; set; }
        //public int TigerTranLogicalRef { get; set; }
    }

    public class SourceTransList
    {
        public List<SourceTrans> sourceTransList { get; set; }
    }

    public class SourceExpense
    {
        public DateTime ExpenseDate { get; set; }
        public string CustomerCode { get; set; }
        public string PacketCode { get; set; }
        public int ExpBranchCode { get; set; }
        public string ServiceCode { get; set; }
        public double Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string ExpCenterCode { get; set; }
        public int TranRefNum { get; set; }
        public string AuxilCode { get; set; }
        public string AuthCode { get; set; }
        public string OrderNo { get; set; }
    }

    public class SourceExpenseList
    {
        public List<SourceExpense> sourceExpenseList { get; set; }
    }

}
