﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.DataOperation.WebService
{
    public class AccountingResponse
    {
        public string Message { get; set; }
        //public SourceTransList Message { get; set; }
        bool IsError { get; set; }
        public int ErrorCode { get; set; }
        public string MessageTypeString { get; set; }
        public string Arguments { get; set; }
    }
    public class AccountingResponseList
    {
        IEnumerable<AccountingResponse> acRList { get; set; }
    }
}
