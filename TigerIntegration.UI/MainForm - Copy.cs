﻿using TigerIntegration.Kernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
//using ModernUIProject.Class;


namespace TigerIntegration.UI
{
    public partial class MainForm11 : MetroForm
    {
        public Guid sessionId { get; set; }
        public bool loggedIn;
        public MainForm11()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                sessionId = SessionManager.Login(txtEMail.Text, txtPassword.Text, 1);
                txtResult.Text = "Login başarılı. SesssionId : " + sessionId.ToString();
                txtResult.Visible = true;
                loggedIn = true;
                this.Close();       
            }

            catch (Exception exc)
            {
                txtResult.Text = exc.Message;
                txtResult.Visible = true;
            }

        }
    }
}
