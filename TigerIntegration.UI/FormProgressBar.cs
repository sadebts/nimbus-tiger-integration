﻿using TigerIntegration.Kernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;

//using ModernUIProject.Class;


namespace TigerIntegration.UI
{

    public partial class FormProgressBar : MetroForm
    {
        int maxValue = 0;
        int curValue = 0;

        public FormProgressBar()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            curValue = Progress.CurrentValue;

           // curValue+;
            metroProgressBar1.Value = curValue;
            lblProgress.Text = string.Format("{0}/{1}", metroProgressBar1.Value, maxValue);

            if (curValue >= maxValue || Progress.Error)
            {
                timer1.Enabled = false;
                this.Close();
            }

        }

        private void FormProgressBar_Load(object sender, EventArgs e)
        {
            lblProgress.Text = "";
            lblProgress.Parent = metroProgressBar1;
            lblProgress.Dock = DockStyle.Fill;
            lblProgress.BackColor = Color.Transparent;

            timer2.Enabled = true;

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            maxValue = Progress.MaxValue;
            if (maxValue > 0)
            {
                timer2.Enabled = false;
                timer1.Enabled = true;

                metroProgressBar1.Maximum = maxValue;
            }

            if (Progress.Error)
            {
                timer2.Enabled = false;
                this.Close();
            }
        }
    }
}
