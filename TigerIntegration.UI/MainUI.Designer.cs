﻿namespace TigerIntegration.UI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cariKartAktarımıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankaFişiAKtarımıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametrelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.parametrelerToolStripMenuItem,
            this.cariKartAktarımıToolStripMenuItem,
            this.bankaFişiAKtarımıToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(107, 20);
            this.toolStripMenuItem1.Text = "Logo Aktarımları";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // cariKartAktarımıToolStripMenuItem
            // 
            this.cariKartAktarımıToolStripMenuItem.Name = "cariKartAktarımıToolStripMenuItem";
            this.cariKartAktarımıToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.cariKartAktarımıToolStripMenuItem.Text = "Cari Kart Aktarımı";
            this.cariKartAktarımıToolStripMenuItem.Click += new System.EventHandler(this.cariKartAktarımıToolStripMenuItem_Click);
            // 
            // bankaFişiAKtarımıToolStripMenuItem
            // 
            this.bankaFişiAKtarımıToolStripMenuItem.Name = "bankaFişiAKtarımıToolStripMenuItem";
            this.bankaFişiAKtarımıToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.bankaFişiAKtarımıToolStripMenuItem.Text = "Banka Fişi AKtarımı";
            this.bankaFişiAKtarımıToolStripMenuItem.Click += new System.EventHandler(this.bankaFişiAKtarımıToolStripMenuItem_Click);
            // 
            // parametrelerToolStripMenuItem
            // 
            this.parametrelerToolStripMenuItem.Name = "parametrelerToolStripMenuItem";
            this.parametrelerToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.parametrelerToolStripMenuItem.Text = "Parametreler";
            this.parametrelerToolStripMenuItem.Click += new System.EventHandler(this.parametrelerToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Tiger Bağlantı";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cariKartAktarımıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankaFişiAKtarımıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parametrelerToolStripMenuItem;
    }
}