﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TigerIntegration.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool loggedIn;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm frmLogin = new MainForm();
            Application.Run(frmLogin);
            
            loggedIn = frmLogin.loggedIn;
            if (loggedIn)
            {
                Application.Run(new frmMain());
            }
        }
    }
}
