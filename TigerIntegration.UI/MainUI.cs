﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TigerIntegration.UI;

namespace TigerIntegration.UI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void cariKartAktarımıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArpCardUI arpCardUI = new ArpCardUI();
            arpCardUI.Show();

        }

        private void bankaFişiAKtarımıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BankVoucher bankVoucherUI = new BankVoucher();
            bankVoucherUI.Show();
        }

        private void parametrelerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParameterUI parameterUI = new ParameterUI();
            parameterUI.Show();

        }
    }
}
