﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;
using TigerIntegration.Orchestration;

namespace TigerIntegration.UI
{
    public partial class viewMain : MetroUserControl
    {
        string aktiveButtonName = "btnBankVoucher";
        viewBankVoucher vwBankVoucher = null;
        viewParameters vwParameters = null;
        viewResetRecord vwResetRecord = null;


        public viewMain(MetroPanel owner)
        {
            InitializeComponent();

            owner.Controls.Add(this);
            this.Dock = DockStyle.Fill;
            owner.StyleManager.Style = MetroFramework.MetroColorStyle.Orange;

            AktiveButtonAppearance(btnBankVoucher);
            vwBankVoucher = new viewBankVoucher(pnlCenter);

        }

        private void metroPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void metroLabel3_MouseHover(object sender, EventArgs e)
        {
            this.btnBankVoucher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnBankVoucher.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
        }

        private void metroLabel3_MouseLeave(object sender, EventArgs e)
        {
            this.btnBankVoucher.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnBankVoucher.ForeColor = System.Drawing.SystemColors.ControlLight;
        }



        private void btnBankVoucher_Click(object sender, EventArgs e)
        {
            if (aktiveButtonName == "btnParameters")
            {
                if (DateTime.Parse(vwParameters.dtpApprovalDate.Text) != new SysParameterOC().Select().approvalDate.Value.Date)
                {
                    DialogResult result = MetroMessageBox.Show(this, "Değişiklik kaydedilmeyecektir.", "Onay tarihi değişikliği", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    if (result == DialogResult.Cancel)
                        return;
                }
            }

            aktiveButtonName = "btnBankVoucher";
            AktiveButtonAppearance(btnBankVoucher);
            PasiveButtonAppearance(btnParameters);
            PasiveButtonAppearance(btnSifirlama);

            if (vwBankVoucher == null)
                vwBankVoucher = new viewBankVoucher(pnlCenter);
            else
                vwBankVoucher.Visible = true;
            if (vwParameters != null)
                vwParameters.Visible = false;
            if (vwResetRecord != null)
                vwResetRecord.Visible = false;

             
        }
 

        private void btnBankVoucher_MouseHover(object sender, EventArgs e)
        {
            AktiveButtonAppearance(sender);
        }

        private void btnBankVoucher_MouseLeave(object sender, EventArgs e)
        {
            if(aktiveButtonName != "btnBankVoucher")
                PasiveButtonAppearance(sender);
            else
                AktiveButtonAppearance(sender);

        }

        private void btnParameters_Click(object sender, EventArgs e)
        {
            DateTime dbDate = new SysParameterOC().Select().approvalDate.Value;
            aktiveButtonName = "btnParameters";
           
            AktiveButtonAppearance(btnParameters);
            PasiveButtonAppearance(btnBankVoucher);
            PasiveButtonAppearance(btnSifirlama);

            if (vwParameters == null)
                vwParameters = new viewParameters(pnlCenter);
            else
                vwParameters.Visible = true;

            if (vwBankVoucher != null)
                vwBankVoucher.Visible = false;
            if (vwResetRecord != null)
                vwResetRecord.Visible = false;

            vwParameters.dtpApprovalDate.Text = dbDate.ToString();

        }

        private void btnParameters_MouseLeave(object sender, EventArgs e)
        {
            if (aktiveButtonName != "btnParameters")
                PasiveButtonAppearance(sender);
            else
                AktiveButtonAppearance(sender);
        }

        private void btnParameters_MouseHover(object sender, EventArgs e)
        {
            AktiveButtonAppearance(sender);
        }

        private void AktiveButtonAppearance(object sender)
        {
            ((MetroLabel)sender).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            ((MetroLabel)sender).ForeColor = System.Drawing.SystemColors.ButtonHighlight;
        }

        private void HoverButtonAppearance(object sender)
        {
            ((MetroLabel)sender).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            ((MetroLabel)sender).ForeColor = System.Drawing.SystemColors.ButtonHighlight;
        }

        private void PasiveButtonAppearance(object sender)
        {
            ((MetroLabel)sender).BackColor = System.Drawing.SystemColors.ControlDarkDark;
            ((MetroLabel)sender).ForeColor = System.Drawing.SystemColors.ControlLight;
        }

        private void HoverButtonBorderAppearance(object sender)
        {
           
        }

        private void btnSifirlama_MouseHover(object sender, EventArgs e)
        {
            AktiveButtonAppearance(sender);
        }

        private void btnSifirlama_MouseLeave(object sender, EventArgs e)
        {
            if (aktiveButtonName != "btnSifirlama")
                PasiveButtonAppearance(sender);
            else
                AktiveButtonAppearance(sender);
        }

        private void btnSifirlama_Click(object sender, EventArgs e)
        {

            if (aktiveButtonName == "btnParameters")
            {
                if (DateTime.Parse(vwParameters.dtpApprovalDate.Text) != new SysParameterOC().Select().approvalDate.Value.Date)
                {
                    DialogResult result = MetroMessageBox.Show(this, "Değişiklik kaydedilmeyecektir.", "Onay tarihi değişikliği", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    if (result == DialogResult.Cancel)
                        return;
                }
            }

            aktiveButtonName = "btnSifirlama";

            AktiveButtonAppearance(btnSifirlama);
            PasiveButtonAppearance(btnParameters);
            PasiveButtonAppearance(btnBankVoucher);

            if (vwResetRecord == null)
                vwResetRecord = new viewResetRecord(pnlCenter);
            else
                vwResetRecord.Visible = true;

            if (vwBankVoucher != null)
                vwBankVoucher.Visible = false;
            if (vwParameters != null)
                vwParameters.Visible = false;
        }
    }
}
