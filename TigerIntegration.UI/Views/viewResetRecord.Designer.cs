﻿namespace TigerIntegration.UI
{
    partial class viewResetRecord
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCenter = new MetroFramework.Controls.MetroPanel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.txtSlipBranchNr = new MetroFramework.Controls.MetroTextBox();
            this.txtSlipRefNumEnd = new MetroFramework.Controls.MetroTextBox();
            this.txtSlipRefNum = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.dtSlipDate = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.pnlCenter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCenter
            // 
            this.pnlCenter.Controls.Add(this.metroLabel4);
            this.pnlCenter.Controls.Add(this.metroComboBox1);
            this.pnlCenter.Controls.Add(this.txtSlipBranchNr);
            this.pnlCenter.Controls.Add(this.txtSlipRefNumEnd);
            this.pnlCenter.Controls.Add(this.txtSlipRefNum);
            this.pnlCenter.Controls.Add(this.metroLabel3);
            this.pnlCenter.Controls.Add(this.metroButton1);
            this.pnlCenter.Controls.Add(this.metroLabel2);
            this.pnlCenter.Controls.Add(this.dtSlipDate);
            this.pnlCenter.Controls.Add(this.metroLabel1);
            this.pnlCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCenter.HorizontalScrollbarBarColor = true;
            this.pnlCenter.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlCenter.HorizontalScrollbarSize = 10;
            this.pnlCenter.Location = new System.Drawing.Point(0, 0);
            this.pnlCenter.Name = "pnlCenter";
            this.pnlCenter.Size = new System.Drawing.Size(567, 335);
            this.pnlCenter.TabIndex = 7;
            this.pnlCenter.VerticalScrollbarBarColor = true;
            this.pnlCenter.VerticalScrollbarHighlightOnWheel = false;
            this.pnlCenter.VerticalScrollbarSize = 10;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(18, 53);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(104, 19);
            this.metroLabel4.TabIndex = 22;
            this.metroLabel4.Text = "Aktarım Kaynağı";
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Items.AddRange(new object[] {
            "Kredi Banka Hareketleri",
            "Fatura Banka Hareketleri",
            "Masraf Hareketleri"});
            this.metroComboBox1.Location = new System.Drawing.Point(128, 43);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(211, 29);
            this.metroComboBox1.TabIndex = 21;
            this.metroComboBox1.UseSelectable = true;
            // 
            // txtSlipBranchNr
            // 
            // 
            // 
            // 
            this.txtSlipBranchNr.CustomButton.Image = null;
            this.txtSlipBranchNr.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.txtSlipBranchNr.CustomButton.Name = "";
            this.txtSlipBranchNr.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSlipBranchNr.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSlipBranchNr.CustomButton.TabIndex = 1;
            this.txtSlipBranchNr.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSlipBranchNr.CustomButton.UseSelectable = true;
            this.txtSlipBranchNr.CustomButton.Visible = false;
            this.txtSlipBranchNr.Lines = new string[0];
            this.txtSlipBranchNr.Location = new System.Drawing.Point(128, 146);
            this.txtSlipBranchNr.MaxLength = 32767;
            this.txtSlipBranchNr.Name = "txtSlipBranchNr";
            this.txtSlipBranchNr.PasswordChar = '\0';
            this.txtSlipBranchNr.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSlipBranchNr.SelectedText = "";
            this.txtSlipBranchNr.SelectionLength = 0;
            this.txtSlipBranchNr.SelectionStart = 0;
            this.txtSlipBranchNr.ShortcutsEnabled = true;
            this.txtSlipBranchNr.Size = new System.Drawing.Size(139, 23);
            this.txtSlipBranchNr.TabIndex = 20;
            this.txtSlipBranchNr.UseSelectable = true;
            this.txtSlipBranchNr.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSlipBranchNr.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtSlipRefNumEnd
            // 
            // 
            // 
            // 
            this.txtSlipRefNumEnd.CustomButton.Image = null;
            this.txtSlipRefNumEnd.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.txtSlipRefNumEnd.CustomButton.Name = "";
            this.txtSlipRefNumEnd.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSlipRefNumEnd.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSlipRefNumEnd.CustomButton.TabIndex = 1;
            this.txtSlipRefNumEnd.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSlipRefNumEnd.CustomButton.UseSelectable = true;
            this.txtSlipRefNumEnd.CustomButton.Visible = false;
            this.txtSlipRefNumEnd.Lines = new string[0];
            this.txtSlipRefNumEnd.Location = new System.Drawing.Point(199, 78);
            this.txtSlipRefNumEnd.MaxLength = 32767;
            this.txtSlipRefNumEnd.Name = "txtSlipRefNumEnd";
            this.txtSlipRefNumEnd.PasswordChar = '\0';
            this.txtSlipRefNumEnd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSlipRefNumEnd.SelectedText = "";
            this.txtSlipRefNumEnd.SelectionLength = 0;
            this.txtSlipRefNumEnd.SelectionStart = 0;
            this.txtSlipRefNumEnd.ShortcutsEnabled = true;
            this.txtSlipRefNumEnd.Size = new System.Drawing.Size(69, 23);
            this.txtSlipRefNumEnd.TabIndex = 19;
            this.txtSlipRefNumEnd.UseSelectable = true;
            this.txtSlipRefNumEnd.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSlipRefNumEnd.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtSlipRefNumEnd.Click += new System.EventHandler(this.txtSlipRefNumEnd_Click);
            // 
            // txtSlipRefNum
            // 
            // 
            // 
            // 
            this.txtSlipRefNum.CustomButton.Image = null;
            this.txtSlipRefNum.CustomButton.Location = new System.Drawing.Point(46, 1);
            this.txtSlipRefNum.CustomButton.Name = "";
            this.txtSlipRefNum.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSlipRefNum.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSlipRefNum.CustomButton.TabIndex = 1;
            this.txtSlipRefNum.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSlipRefNum.CustomButton.UseSelectable = true;
            this.txtSlipRefNum.CustomButton.Visible = false;
            this.txtSlipRefNum.Lines = new string[0];
            this.txtSlipRefNum.Location = new System.Drawing.Point(128, 78);
            this.txtSlipRefNum.MaxLength = 32767;
            this.txtSlipRefNum.Name = "txtSlipRefNum";
            this.txtSlipRefNum.PasswordChar = '\0';
            this.txtSlipRefNum.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSlipRefNum.SelectedText = "";
            this.txtSlipRefNum.SelectionLength = 0;
            this.txtSlipRefNum.SelectionStart = 0;
            this.txtSlipRefNum.ShortcutsEnabled = true;
            this.txtSlipRefNum.Size = new System.Drawing.Size(68, 23);
            this.txtSlipRefNum.TabIndex = 18;
            this.txtSlipRefNum.UseSelectable = true;
            this.txtSlipRefNum.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSlipRefNum.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(32, 146);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(85, 19);
            this.metroLabel3.TabIndex = 17;
            this.metroLabel3.Text = "SlipBranchNr";
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(155, 186);
            this.metroButton1.Margin = new System.Windows.Forms.Padding(2);
            this.metroButton1.MinimumSize = new System.Drawing.Size(0, 16);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(113, 23);
            this.metroButton1.TabIndex = 9;
            this.metroButton1.Text = "Kayıtları sıfırla";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click_1);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(32, 112);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(57, 19);
            this.metroLabel2.TabIndex = 5;
            this.metroLabel2.Text = "SlipDate";
            // 
            // dtSlipDate
            // 
            this.dtSlipDate.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dtSlipDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtSlipDate.Location = new System.Drawing.Point(128, 112);
            this.dtSlipDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtSlipDate.MinimumSize = new System.Drawing.Size(0, 25);
            this.dtSlipDate.Name = "dtSlipDate";
            this.dtSlipDate.Size = new System.Drawing.Size(140, 25);
            this.dtSlipDate.TabIndex = 4;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(32, 79);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(78, 19);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "SlipRefNum";
            // 
            // viewResetRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlCenter);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "viewResetRecord";
            this.Size = new System.Drawing.Size(567, 335);
            this.pnlCenter.ResumeLayout(false);
            this.pnlCenter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroDateTime dtSlipDate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroPanel pnlCenter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtSlipRefNum;
        private MetroFramework.Controls.MetroTextBox txtSlipBranchNr;
        private MetroFramework.Controls.MetroTextBox txtSlipRefNumEnd;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
    }
}
