﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;


namespace TigerIntegration.UI
{
    public partial class viewMainBck : MetroUserControl
    {
        public viewMainBck(MetroPanel owner)
        {
            InitializeComponent();

            owner.Controls.Add(this);
            //this.Width = owner.Width;
            //this.Height = owner.Height;
            //this.Location = new Point(0, 0);
            //this.BringToFront();
            this.Dock = DockStyle.Fill;
            owner.StyleManager.Style = MetroFramework.MetroColorStyle.Orange;

        }
    }
}
