﻿namespace TigerIntegration.UI
{
    partial class BankVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBankVoucherInsert = new System.Windows.Forms.Button();
            this.txtLogoPassword = new System.Windows.Forms.TextBox();
            this.lblLogoPassword = new System.Windows.Forms.Label();
            this.lblLogoUserName = new System.Windows.Forms.Label();
            this.txtLogoUserName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnBankVoucherInsert
            // 
            this.btnBankVoucherInsert.Location = new System.Drawing.Point(30, 164);
            this.btnBankVoucherInsert.Name = "btnBankVoucherInsert";
            this.btnBankVoucherInsert.Size = new System.Drawing.Size(118, 23);
            this.btnBankVoucherInsert.TabIndex = 9;
            this.btnBankVoucherInsert.Text = "Banka Fişi Aktar";
            this.btnBankVoucherInsert.UseVisualStyleBackColor = true;
            this.btnBankVoucherInsert.Click += new System.EventHandler(this.btnBankVoucherInsert_Click);
            // 
            // txtLogoPassword
            // 
            this.txtLogoPassword.Location = new System.Drawing.Point(142, 80);
            this.txtLogoPassword.Name = "txtLogoPassword";
            this.txtLogoPassword.Size = new System.Drawing.Size(100, 20);
            this.txtLogoPassword.TabIndex = 8;
            // 
            // lblLogoPassword
            // 
            this.lblLogoPassword.AutoSize = true;
            this.lblLogoPassword.Location = new System.Drawing.Point(27, 87);
            this.lblLogoPassword.Name = "lblLogoPassword";
            this.lblLogoPassword.Size = new System.Drawing.Size(55, 13);
            this.lblLogoPassword.TabIndex = 7;
            this.lblLogoPassword.Text = "Logo Şifre";
            // 
            // lblLogoUserName
            // 
            this.lblLogoUserName.AutoSize = true;
            this.lblLogoUserName.Location = new System.Drawing.Point(27, 39);
            this.lblLogoUserName.Name = "lblLogoUserName";
            this.lblLogoUserName.Size = new System.Drawing.Size(91, 13);
            this.lblLogoUserName.TabIndex = 6;
            this.lblLogoUserName.Text = "Logo Kullanıcı Adı";
            // 
            // txtLogoUserName
            // 
            this.txtLogoUserName.Location = new System.Drawing.Point(142, 32);
            this.txtLogoUserName.Name = "txtLogoUserName";
            this.txtLogoUserName.Size = new System.Drawing.Size(100, 20);
            this.txtLogoUserName.TabIndex = 5;
            // 
            // BankVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnBankVoucherInsert);
            this.Controls.Add(this.txtLogoPassword);
            this.Controls.Add(this.lblLogoPassword);
            this.Controls.Add(this.lblLogoUserName);
            this.Controls.Add(this.txtLogoUserName);
            this.Name = "BankVoucher";
            this.Text = "BankVoucher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBankVoucherInsert;
        private System.Windows.Forms.TextBox txtLogoPassword;
        private System.Windows.Forms.Label lblLogoPassword;
        private System.Windows.Forms.Label lblLogoUserName;
        private System.Windows.Forms.TextBox txtLogoUserName;
    }
}