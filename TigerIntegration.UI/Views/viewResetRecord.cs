﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;
using TigerIntegration.Orchestration;
using TigerIntegration.Kernel;
using System.Threading;
using System.Configuration;

namespace TigerIntegration.UI
{
    public partial class viewResetRecord : MetroUserControl
    {
        FormProgressBar frmProgressBar = null;
        Thread thread1;
        Thread thread2;


        public viewResetRecord(MetroPanel owner)
        {
            InitializeComponent();

            owner.Controls.Add(this);
            this.Dock = DockStyle.Fill;
            owner.StyleManager.Style = MetroFramework.MetroColorStyle.Orange;

            if (ConfigurationManager.AppSettings["integrationType"] == "1")
                this.metroComboBox1.SelectedItem = "Kredi Banka Hareketleri";
            else if (ConfigurationManager.AppSettings["integrationType"] == "2")
                this.metroComboBox1.SelectedItem = "Fatura Banka Hareketleri";
            else if (ConfigurationManager.AppSettings["integrationType"] == "3")
                this.metroComboBox1.SelectedItem = "Masraf Hareketleri";
        }
        
      

        private void metroButton1_Click_1(object sender, EventArgs e)
        {
            BankVoucherOC bn = new BankVoucherOC();

            if (metroComboBox1.SelectedItem.ToString() == "Kredi Banka Hareketleri")
            {
                bn.ResetRecordsOnWeb(int.Parse(txtSlipBranchNr.Text), int.Parse(txtSlipRefNum.Text), int.Parse(txtSlipRefNumEnd.Text), dtSlipDate.Value.Date, false);
            }
            else if (metroComboBox1.SelectedItem.ToString() == "Fatura Banka Hareketleri")
            {
                bn.ResetRecordsOnWeb(int.Parse(txtSlipBranchNr.Text), int.Parse(txtSlipRefNum.Text), int.Parse(txtSlipRefNumEnd.Text), dtSlipDate.Value.Date, true);
            }
            else
            {
                bn.ResetExpenseRecordsOnWeb(int.Parse(txtSlipRefNum.Text), int.Parse(txtSlipRefNumEnd.Text), dtSlipDate.Value.Date);
            }
            
            MetroMessageBox.Show(this, "İşlem tamamlandı", "Kayıt Sıfırlama", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void pnlCenter_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtSlipRefNumEnd_Click(object sender, EventArgs e)
        {

        }
    }
}
