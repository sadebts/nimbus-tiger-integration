﻿namespace TigerIntegration.UI
{
    partial class viewMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCenter = new MetroFramework.Controls.MetroPanel();
            this.metroPanelTop = new MetroFramework.Controls.MetroPanel();
            this.btnSifirlama = new MetroFramework.Controls.MetroLabel();
            this.btnParameters = new MetroFramework.Controls.MetroLabel();
            this.btnBankVoucher = new MetroFramework.Controls.MetroLabel();
            this.metroPanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCenter
            // 
            this.pnlCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCenter.HorizontalScrollbarBarColor = true;
            this.pnlCenter.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlCenter.HorizontalScrollbarSize = 10;
            this.pnlCenter.Location = new System.Drawing.Point(0, 46);
            this.pnlCenter.Name = "pnlCenter";
            this.pnlCenter.Size = new System.Drawing.Size(567, 289);
            this.pnlCenter.TabIndex = 7;
            this.pnlCenter.VerticalScrollbarBarColor = true;
            this.pnlCenter.VerticalScrollbarHighlightOnWheel = false;
            this.pnlCenter.VerticalScrollbarSize = 10;
            // 
            // metroPanelTop
            // 
            this.metroPanelTop.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.metroPanelTop.Controls.Add(this.btnSifirlama);
            this.metroPanelTop.Controls.Add(this.btnParameters);
            this.metroPanelTop.Controls.Add(this.btnBankVoucher);
            this.metroPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanelTop.HorizontalScrollbarBarColor = true;
            this.metroPanelTop.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanelTop.HorizontalScrollbarSize = 6;
            this.metroPanelTop.Location = new System.Drawing.Point(0, 0);
            this.metroPanelTop.Margin = new System.Windows.Forms.Padding(2);
            this.metroPanelTop.Name = "metroPanelTop";
            this.metroPanelTop.Size = new System.Drawing.Size(567, 46);
            this.metroPanelTop.TabIndex = 10;
            this.metroPanelTop.UseCustomBackColor = true;
            this.metroPanelTop.VerticalScrollbarBarColor = true;
            this.metroPanelTop.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanelTop.VerticalScrollbarSize = 7;
            // 
            // btnSifirlama
            // 
            this.btnSifirlama.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnSifirlama.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.btnSifirlama.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnSifirlama.Location = new System.Drawing.Point(333, 3);
            this.btnSifirlama.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnSifirlama.Name = "btnSifirlama";
            this.btnSifirlama.Size = new System.Drawing.Size(108, 38);
            this.btnSifirlama.TabIndex = 16;
            this.btnSifirlama.Text = "Kayıt Sıfırlama";
            this.btnSifirlama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSifirlama.UseCustomBackColor = true;
            this.btnSifirlama.UseCustomForeColor = true;
            this.btnSifirlama.Click += new System.EventHandler(this.btnSifirlama_Click);
            this.btnSifirlama.MouseLeave += new System.EventHandler(this.btnSifirlama_MouseLeave);
            this.btnSifirlama.MouseHover += new System.EventHandler(this.btnSifirlama_MouseHover);
            // 
            // btnParameters
            // 
            this.btnParameters.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnParameters.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.btnParameters.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnParameters.Location = new System.Drawing.Point(221, 3);
            this.btnParameters.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnParameters.Name = "btnParameters";
            this.btnParameters.Size = new System.Drawing.Size(108, 38);
            this.btnParameters.TabIndex = 15;
            this.btnParameters.Text = "Parametreler";
            this.btnParameters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnParameters.UseCustomBackColor = true;
            this.btnParameters.UseCustomForeColor = true;
            this.btnParameters.Click += new System.EventHandler(this.btnParameters_Click);
            this.btnParameters.MouseLeave += new System.EventHandler(this.btnParameters_MouseLeave);
            this.btnParameters.MouseHover += new System.EventHandler(this.btnParameters_MouseHover);
            // 
            // btnBankVoucher
            // 
            this.btnBankVoucher.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnBankVoucher.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.btnBankVoucher.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnBankVoucher.Location = new System.Drawing.Point(2, 3);
            this.btnBankVoucher.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnBankVoucher.Name = "btnBankVoucher";
            this.btnBankVoucher.Size = new System.Drawing.Size(215, 38);
            this.btnBankVoucher.TabIndex = 13;
            this.btnBankVoucher.Text = "Nimbus Hareketleri Aktarımı";
            this.btnBankVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnBankVoucher.UseCustomBackColor = true;
            this.btnBankVoucher.UseCustomForeColor = true;
            this.btnBankVoucher.Click += new System.EventHandler(this.btnBankVoucher_Click);
            this.btnBankVoucher.MouseLeave += new System.EventHandler(this.btnBankVoucher_MouseLeave);
            this.btnBankVoucher.MouseHover += new System.EventHandler(this.btnBankVoucher_MouseHover);
            // 
            // viewMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlCenter);
            this.Controls.Add(this.metroPanelTop);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "viewMain";
            this.Size = new System.Drawing.Size(567, 335);
            this.metroPanelTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroPanel pnlCenter;
        private MetroFramework.Controls.MetroPanel metroPanelTop;
        private MetroFramework.Controls.MetroLabel btnBankVoucher;
        private MetroFramework.Controls.MetroLabel btnParameters;
        private MetroFramework.Controls.MetroLabel btnSifirlama;
    }
}
