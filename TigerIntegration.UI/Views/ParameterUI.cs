﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TigerIntegration.Entity;
using TigerIntegration.Orchestration;

namespace TigerIntegration.UI
{
    public partial class ParameterUI : Form
    {
        SysParameterOC parameterOC = new SysParameterOC();
        SysParameterVM sysParameterVM = new SysParameterVM();
        DateTime vDateTime;

        public ParameterUI()
        {
            InitializeComponent();
            sysParameterVM = parameterOC.Select();
            this.dtpApprovalDate.Text = sysParameterVM.approvalDate.ToString();
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DateTime.TryParse(dtpApprovalDate.Text, out vDateTime);
            sysParameterVM.approvalDate = vDateTime;
            parameterOC.Update(sysParameterVM);
            MessageBox.Show("Güncelleme yapıldı.");
            this.Close();
        }
    }
}
