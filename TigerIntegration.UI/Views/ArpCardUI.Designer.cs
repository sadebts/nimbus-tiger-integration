﻿namespace TigerIntegration.UI
{
    partial class ArpCardUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLogoUserName = new System.Windows.Forms.TextBox();
            this.lblLogoUserName = new System.Windows.Forms.Label();
            this.lblLogoPassword = new System.Windows.Forms.Label();
            this.txtLogoPassword = new System.Windows.Forms.TextBox();
            this.btnArpCardInsert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLogoUserName
            // 
            this.txtLogoUserName.Location = new System.Drawing.Point(127, 23);
            this.txtLogoUserName.Name = "txtLogoUserName";
            this.txtLogoUserName.Size = new System.Drawing.Size(100, 20);
            this.txtLogoUserName.TabIndex = 0;
            // 
            // lblLogoUserName
            // 
            this.lblLogoUserName.AutoSize = true;
            this.lblLogoUserName.Location = new System.Drawing.Point(12, 30);
            this.lblLogoUserName.Name = "lblLogoUserName";
            this.lblLogoUserName.Size = new System.Drawing.Size(91, 13);
            this.lblLogoUserName.TabIndex = 1;
            this.lblLogoUserName.Text = "Logo Kullanıcı Adı";
            // 
            // lblLogoPassword
            // 
            this.lblLogoPassword.AutoSize = true;
            this.lblLogoPassword.Location = new System.Drawing.Point(12, 78);
            this.lblLogoPassword.Name = "lblLogoPassword";
            this.lblLogoPassword.Size = new System.Drawing.Size(55, 13);
            this.lblLogoPassword.TabIndex = 2;
            this.lblLogoPassword.Text = "Logo Şifre";
            // 
            // txtLogoPassword
            // 
            this.txtLogoPassword.Location = new System.Drawing.Point(127, 71);
            this.txtLogoPassword.Name = "txtLogoPassword";
            this.txtLogoPassword.Size = new System.Drawing.Size(100, 20);
            this.txtLogoPassword.TabIndex = 3;
            

        }

        #endregion

        private System.Windows.Forms.TextBox txtLogoUserName;
        private System.Windows.Forms.Label lblLogoUserName;
        private System.Windows.Forms.Label lblLogoPassword;
        private System.Windows.Forms.TextBox txtLogoPassword;
        private System.Windows.Forms.Button btnArpCardInsert;
    }
}