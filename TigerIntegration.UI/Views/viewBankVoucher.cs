﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;
using TigerIntegration.Orchestration;
using TigerIntegration.Kernel;
using System.Threading;
using System.Configuration;

namespace TigerIntegration.UI
{
    public partial class viewBankVoucher : MetroUserControl
    {
        FormProgressBar frmProgressBar = null;
        Thread thread1;
        Thread thread2;
        int transType;

        public viewBankVoucher(MetroPanel owner)
        {
            InitializeComponent();

            owner.Controls.Add(this);
            this.Dock = DockStyle.Fill;
            owner.StyleManager.Style = MetroFramework.MetroColorStyle.Orange;

            if (ConfigurationManager.AppSettings["integrationType"] == "1")
                this.metroComboBox1.SelectedItem = "Kredi Banka Hareketleri";
            else if (ConfigurationManager.AppSettings["integrationType"] == "2")
                this.metroComboBox1.SelectedItem = "Fatura Banka Hareketleri";
            else if (ConfigurationManager.AppSettings["integrationType"] == "3")
                this.metroComboBox1.SelectedItem = "Masraf Hareketleri";

            if (ConfigurationManager.AppSettings["writeDebugLog"] == "1")
                timer1.Start();
        }

        private void btnBankVoucherInsert_Click(object sender, EventArgs e)
        {
            if (metroDateTime1.Value.Date > metroDateTime2.Value.Date)
                MetroMessageBox.Show(this, "Başlangıç tarihi bitişten büyük olamaz","Tarih hatası",MessageBoxButtons.OK,MessageBoxIcon.Error);
            else
            {
                
                string msgString, msgSourceStr;
                if (metroComboBox1.SelectedItem.ToString() == "Kredi Banka Hareketleri")
                {
                    transType = 1;
                    msgSourceStr = "kredi banka hareketleri";
                }
                else if (metroComboBox1.SelectedItem.ToString() == "Fatura Banka Hareketleri")
                {
                    transType = 2;
                    msgSourceStr = "faturalar";
                }
                else
                {
                    transType = 3;
                    msgSourceStr = "masraflar";
                }

                msgString = metroDateTime1.Value.Date.Day.ToString() + "." + metroDateTime1.Value.Date.Month.ToString() + "." + metroDateTime1.Value.Date.Year.ToString()
                                + " - " + metroDateTime2.Value.Date.Day.ToString() + "." + metroDateTime2.Value.Date.Month.ToString() + "." + metroDateTime2.Value.Date.Year.ToString()
                                + " tarihleri arasındaki " + msgSourceStr +  " Tiger’a aktarılacaktır.";

                //thread2 = new Thread(new ThreadStart(showProgressForm));
                DialogResult result = MetroMessageBox.Show(this,msgString,  "İşlem Onayı", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                    return;

                thread1 = new Thread(new ThreadStart(bankVoucherInsert));
                thread1.Start();
                //thread2.Start();
                showProgressForm();
            }
        }

        private void bankVoucherInsert()
        {
            BankVoucherOC bankVoucherOC = new BankVoucherOC();

            try
            {
                //   bankVoucherInsert.InsertFromCsv();
                //   bankVoucherInsert.InsertFromWebService(metroDateTime1.Value.Date, metroDateTime2.Value.Date);
                //bankVoucherInsert.InsertBankVoucherWithInvoice(metroDateTime1.Value.Date, metroDateTime2.Value.Date);
                //  bankVoucherInsert.UpdateRecords2Web4Invoice(181);
                
                if (transType == 1)
                    //bankVoucherOC.InsertBankVoucherWeb(metroDateTime1.Value.Date, metroDateTime2.Value.Date);
                    bankVoucherOC.InsertBankCreditTransactions(metroDateTime1.Value.Date, metroDateTime2.Value.Date); 
                else if (transType == 2)
                    bankVoucherOC.InsertBankVoucherWithInvoiceWeb(metroDateTime1.Value.Date, metroDateTime2.Value.Date);
                else
                    bankVoucherOC.InsertExpenseWeb(metroDateTime1.Value.Date, metroDateTime2.Value.Date);

                if (bankVoucherOC.logoErrorMessage != null && bankVoucherOC.logoErrorMessage != "")
                {
                    Progress.Error = true;
                    MetroMessageBox.Show(this, "LOGO Tiger Hatası:" + bankVoucherOC.logoErrorMessage, "Kayıt aktarma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Progress.Error = true;
                MetroMessageBox.Show(this, ex.Message , "Kayıt aktarma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (Progress.ErrorMessage != "")
                {
                    MetroMessageBox.Show(this, Progress.ErrorMessage, "Kayıt aktarma", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void showProgressForm()
        {
            frmProgressBar = new FormProgressBar();
            frmProgressBar.ShowDialog();
            if (Progress.Error == false)
            {
                if(Progress.NoDataFound)
                    MetroMessageBox.Show(this, "İlgili tarihlerde aktarılacak kayıt bulunamadı.", "Kayıt aktarma", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                else
                    MetroMessageBox.Show(this, "İşlem tamamlandı.", "Kayıt aktarma", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            
            Progress.MaxValue = 0;
            Progress.CurrentValue = 0;
            Progress.Error = false;
        }


        private void metroButton1_Click(object sender, EventArgs e)
        {
            MD5Manager md5 = new MD5Manager();
            string encr;
            string decr;
            encr = md5.EncryptData("Murat", "Sayıbakan");
            MessageBox.Show(encr);

            MessageBox.Show(md5.DecryptData("Murat", encr));


        }

        private void pnlCenter_Paint(object sender, PaintEventArgs e)
        {

        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click_1(object sender, EventArgs e)
        {
            

        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(ConfigurationManager.AppSettings["writeDebugLog"] == "1")
            {
                metroTextBox1.Visible = true;
                metroTextBox1.Text = Progress.ErrorMessage;
            }
        }
    }
}
