﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework;
using MetroFramework.Forms;
using TigerIntegration.Entity;
using TigerIntegration.Orchestration;



namespace TigerIntegration.UI
{
    public partial class viewParameters : MetroUserControl
    {
        SysParameterOC parameterOC = new SysParameterOC();
        SysParameterVM sysParameterVM = new SysParameterVM();
        DateTime vDateTime;

        public viewParameters(MetroPanel owner)
        {
            InitializeComponent();

            owner.Controls.Add(this);
            this.Dock = DockStyle.Fill;
            owner.StyleManager.Style = MetroFramework.MetroColorStyle.Orange;
            try
            {
                sysParameterVM = parameterOC.Select();
                this.dtpApprovalDate.Text = sysParameterVM.approvalDate.ToString();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime.TryParse(dtpApprovalDate.Text, out vDateTime);
                sysParameterVM.approvalDate = vDateTime;
                parameterOC.Update(sysParameterVM);

                MetroMessageBox.Show(this, "Güncelleme yapıldı.", "Parametre güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Parametre güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }
    }
}
