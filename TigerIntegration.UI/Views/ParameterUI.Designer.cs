﻿namespace TigerIntegration.UI
{
    partial class ParameterUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpApprovalDate = new System.Windows.Forms.DateTimePicker();
            this.lblApproveDate = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dtpApprovalDate
            // 
            this.dtpApprovalDate.Location = new System.Drawing.Point(95, 70);
            this.dtpApprovalDate.Name = "dtpApprovalDate";
            this.dtpApprovalDate.Size = new System.Drawing.Size(200, 20);
            this.dtpApprovalDate.TabIndex = 0;
            // 
            // lblApproveDate
            // 
            this.lblApproveDate.AutoSize = true;
            this.lblApproveDate.Location = new System.Drawing.Point(12, 77);
            this.lblApproveDate.Name = "lblApproveDate";
            this.lblApproveDate.Size = new System.Drawing.Size(61, 13);
            this.lblApproveDate.TabIndex = 1;
            this.lblApproveDate.Text = "Onay Tarihi";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 127);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Kaydet";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(113, 127);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Vazgeç";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ParameterUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 272);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblApproveDate);
            this.Controls.Add(this.dtpApprovalDate);
            this.Name = "ParameterUI";
            this.Text = "Parametreler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpApprovalDate;
        private System.Windows.Forms.Label lblApproveDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}