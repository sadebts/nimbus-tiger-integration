﻿using TigerIntegration.Kernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
//using ModernUIProject.Class;


namespace TigerIntegration.UI
{
    public partial class MainForm : MetroForm
    {
        public Guid sessionId { get; set; }
        public bool loggedIn;
        viewLogin vwLogin = null;
        viewMain vwMain = null;

        public MainForm()
        {
            InitializeComponent();
            this.StyleManager = msmMain;
            //this.Shown += MainForm_Shown;

            vwLogin = new viewLogin(mainPanel);
            vwLogin.LogInSuccess += _login_LogInSuccess;
            vwLogin.swipe();

        }


        void _login_LogInSuccess(object sender, EventArgs e)
        {
            lnlClose.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            vwMain = new viewMain(mainPanel);
            vwLogin.swipe(false);
        }

        private void lnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lnlClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

       
    }
}
