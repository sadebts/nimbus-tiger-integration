﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SourceTrans
    {
        public int SlipType;
        public DateTime SlipDate;
        public int SlipRefNumber;
        public int SlipBranchNr;
        public int TranAccType;
        public int TranSendType;
        public string TranDocumentNo;
        public string BankAccountCode;
        public string CrossAccountCode;
        public double TranTotal;
        public string TransExp;
        public int TranSerialNumber;
        public DateTime OldSlipDate;
        public int OldSlipRefNumber;
        public int OldSlipBranchNr;
    }
}
