﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SysParameterVM
    {
        public int logicalRef { get; set; }

        public Nullable<System.DateTime> approvalDate { get; set; }

    }

}
