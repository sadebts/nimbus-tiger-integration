﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class CountryVM
    {
        public int LogicalRef { get; set; }
        public int CountryNr { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class CityVM
    {
        public int LogicalRef { get; set; }
        public int CountryNr { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class TownVM
    {
        public int LogicalRef { get; set; }
        public int CityRef { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }


    public class SrvCard
    {
        public int LogicalRef { get; set; }
        public string Code { get; set; }
        public double VAT { get; set; }
    }


}
