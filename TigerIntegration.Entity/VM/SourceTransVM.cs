﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SourceTransVM
    {
        public int SlipType;
        public DateTime SlipDate;
        public int SlipRefNumber;
        public int SlipBranchNr;
        public int TranAccType;
        public int TranSendType;
        public string TranDocumentNo;
        public string BankAccountCode;
        public string TranAccountCode;
        public double TranTotal;
        public string TranExp;
        public int TranSerialNumber;
        public DateTime? OldSlipDate;
        public int OldSlipRefNumber;
        public int OldSlipBranchNr;
        public int TigerTranLogicalRef;
        public string TranSign;
        public string TranCurrency;
        public int ficheType;
        public string ficheNo;
        public DateTime OriginalSlipDate { get; set; }

        public int PayTransRef;
        public int InvoiceRef;

        public string InvoiceCode;

        public short CustType { get; set; }
        public string CustTaxOffice { get; set; }
        // public short CustSendType { get; set; }
        public string CustCountry { get; set; }
        public string CustCity { get; set; }
        public string CustTown { get; set; }
        public string CustTaxNr { get; set; }
        public string CustName { get; set; }
        public string CustSurname { get; set; }
        public string CustTCKN { get; set; }
        public string CustTitle { get; set; }
        public string CustAddress1 { get; set; }
        public string CustAddress2 { get; set; }
        public string CustEMail { get; set; }
        public string ServiceCode { get; set; }

        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string TownName { get; set; }

    }

    public class ExpenseVM
    {
        public DateTime InvDate;
        public string InvCustomerCode;
        public string InvPacketCode ;
        public int InvBranchNr;
        public string LineServiceCode { get; set; }
        public double LineTotal;
        public string LineCurrency;
        public string LineDesc;
        public string LineExpCenterCode;
        public int LineRefNumber ;
        public string AuxilCode { get; set; }
        public string AuthCode { get; set; }
        public int LogoPayTransRef;
        public int LogoInvoiceRef;
        public string LogoInvoiceCode;
        public string InvDocNumber;

    }

    public class IncomingRefPayTransRef
    {
        public int incomingRef { get; set; }
        public int payTransRef { get; set; }
    }
}
