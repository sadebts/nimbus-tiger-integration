﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SysUserVM
    {
        public Guid sessionId { get; set; }
        public int sysCustomerId { get; set; }
        public int sysUserId { get; set; }
        public string eMail { get; set; }
        public string password { get; set; }
        public Nullable<byte> status { get; set; }
        public Nullable<int> createdBy { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<int> modifiedBy { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
    }
}
