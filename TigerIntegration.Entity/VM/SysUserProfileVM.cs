﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SysUserProfileVM
    {
        public int sysCustomerId { get; set; }
        public int sysUserId { get; set; }
        public int sysUserProfileId { get; set; }
        public int profileId { get; set; }
        public Nullable<byte> status { get; set; }
        public Nullable<int> createdBy { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<int> modifiedBy { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
    }
}
