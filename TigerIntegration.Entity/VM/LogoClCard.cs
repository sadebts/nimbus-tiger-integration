﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class LogoClCard
    {
        public string Code { get; set; }
        public short AcceptEInvoice { get; set; }
        public string EMail { get; set; }
        public short EInvoiceTyp { get; set; }
        public short IsPersComp { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public short ProfileId { get; set; }
        public short IsPerCurr { get; set; }
    }
}
