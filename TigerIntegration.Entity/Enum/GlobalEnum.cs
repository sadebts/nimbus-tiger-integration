﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity.Enum
{
    public enum YesNo
    {
        No,
        Yes
    }
}
