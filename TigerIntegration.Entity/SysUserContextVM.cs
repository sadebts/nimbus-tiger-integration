﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Entity
{
    public class SysUserContextVM
    {
        public SysUserVM sysUserVM { get; set; }
        public List<int> profileList { get; set; } //ProfileId bilgilerini tutan liste   
        public  SysUserContextVM()
        {
            sysUserVM = new SysUserVM();
            profileList = new List<int>();
        }

    }
}
