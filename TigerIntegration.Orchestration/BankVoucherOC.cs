﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TigerIntegration.Entity;
using UnityObjects;
using System.Configuration;
using TigerIntegration.DataOperation;
using System.Diagnostics;
using TigerIntegration.Kernel;
using RestSharp;
using TigerIntegration.DataOperation.WebService;
using TigerIntegration.Orchestration.DiyaLogoSVC;
using Newtonsoft.Json;

namespace TigerIntegration.Orchestration
{
    public class BankVoucherOC
    {
        public string logoUserName = ConfigurationManager.AppSettings["objectUserName"];
        public string logoPassword = ConfigurationManager.AppSettings["objectPassword"];
        public int logoFirmNr = Convert.ToInt32(ConfigurationManager.AppSettings["firmNr"]);
        public int logoPeriodNr = Convert.ToInt32(ConfigurationManager.AppSettings["periodNr"]);
        public string diyalogoUserName = ConfigurationManager.AppSettings["diyaLogoUserName"];
        public string diyalogoPassword = ConfigurationManager.AppSettings["diyaLogoPassword"];
        UnityApplication unityApp = new UnityApplication();
        public ValidateErrors err;
        public string logoErrorMessage;
        bool firstIteration = true;
        DateTime prevSlipDate = DateTime.MinValue;
        DateTime approvalDate = DateTime.MinValue;
        DateTime oldSlipDate = DateTime.MinValue;
        int prevSlipRefNumber = 0, prevSlipBranchNr = 0, prevSlipType = 0, firstSlipRefNumber = 0, lastSlipRefNumber = 0;
        string prevBankAccountCode = "";
        string prevAccountCode = "";
        string prevAuxilCode = "";
        string prevAuthCode = "";

        DateTime prevInvDate = DateTime.MinValue;
        string prevInvCustomerCode = "";
        int prevInvBranchNr = 0;
        string prevInvPackedCode = "";
        string prevInvDocNumber = "";


        List<SourceTransVM> InProcessList = new List<SourceTransVM>();
        List<ExpenseVM> InProcessExpenseList = new List<ExpenseVM>();
        List<SourceTransVM> oldTranList = new List<SourceTransVM>();
        List<SourceTransVM> invoiceLineList = new List<SourceTransVM>();
        string logFileName = "", webLogFileName = "", debugFileName = "";
        int count = 0;
        Token token;
        int slipRefNumbersLimit = Convert.ToInt32(ConfigurationManager.AppSettings["slipRefNumbersLimit"]);
        int maxLineCount4Vouchers = Convert.ToInt32(ConfigurationManager.AppSettings["maxLineCount4Vouchers"]);
        int proccessedSlipRefNumber = 0;
        List<CountryVM> countryVMList = new List<CountryVM>();
        List<CityVM> cityVMList = new List<CityVM>();
        List<TownVM> townVMList = new List<TownVM>();

        CountryVM countryVM = new CountryVM();
        CityVM cityVM = new CityVM();
        TownVM townVM = new TownVM();


        public BankVoucherOC()
        {
            approvalDate = (DateTime)new SysParameterOC().Select().approvalDate;
            logFileName = GetLogFileName();
            webLogFileName = GetLogFileName(1);
            debugFileName = GetLogFileName(2);

            countryVMList = new BasicDataDO().SelectCountryList();
            cityVMList = new BasicDataDO().SelectCityList();
            townVMList = new BasicDataDO().SelectTownList();

            if (slipRefNumbersLimit == 0)
                slipRefNumbersLimit = 999999999;
            Progress.NoDataFound = false;
            WriteDebugLog("BankVoucherOC nesnesini oluşturduk.");
        }

        public void InsertFromCsv()
        {
            if (unityApp.Connect()

                && unityApp.UserLogin(logoUserName, logoPassword)
                && unityApp.CompanyLogin(logoFirmNr))
            {
                string csvFile = ConfigurationManager.AppSettings["csvFile"];
                var lines = File.ReadAllLines(csvFile).Select(x => x.Split(';'))
                            .Skip(1) //ilk satırı atlıyoruz
                            .ToList();
                var query = lines.Select(s => new SourceTransVM
                {

                    SlipDate = DateTime.Parse(s[0]),
                    SlipType = int.Parse(s[1]),
                    SlipRefNumber = int.Parse(s[2]),
                    SlipBranchNr = int.Parse(s[3]),
                    TranAccType = int.Parse(s[4]),
                    TranSendType = int.Parse(s[5]),

                    // BankAccountCode = s[7],
                    TranAccountCode = s[6],
                    TranTotal = double.Parse(s[7]),
                    TranDocumentNo = s[8],
                    TranExp = s[9],
                    TranSerialNumber = int.Parse(s[10]),
                    OldSlipDate = s[11] != "" ? DateTime.Parse(s[11]) : DateTime.MinValue,
                    OldSlipRefNumber = s[12] != "" ? int.Parse(s[12]) : 0,
                    OldSlipBranchNr = s[13] != "" ? int.Parse(s[13]) : 0,
                    TigerTranLogicalRef = 0,
                    PayTransRef = 0,
                    InvoiceRef = 0,

                    CustType = short.Parse(s[14]),
                    CustCountry = s[15],
                    CustCity = s[16],
                    CustTown = s[17],
                    CustTaxNr = s[18],
                    CustName = s[19],
                    CustSurname = s[20],
                    CustTCKN = s[21],
                    CustTitle = s[22],
                    CustAddress1 = s[23],
                    CustAddress2 = s[24],
                    CustEMail = s[25],
                    ServiceCode = s[26],
                    TranSign = s[27]
                }
                                        );

                List<SourceTransVM> allList = query.ToList();
                Progress.MaxValue = allList.Count;
                Progress.CurrentValue = 0;
                ProcessList(allList, true);
            }
            else //of if (unityApp.Connect() && unityApp.UserLogin(this.logoUserName, this.logoPassword) && unityApp.CompanyLogin(this.logoFirmNr)) 
            {
                if (unityApp.GetLastErrorString() != null)
                    logoErrorMessage = unityApp.GetLastErrorString().ToString();
            }

            if (unityApp.CompanyLoggedIn)
                unityApp.CompanyLogout();
            if (unityApp.LoggedIn)
                unityApp.UserLogout();
            if (unityApp.Connected)
                unityApp.Disconnect();
        }

        private void ProcessList(List<SourceTransVM> allList, bool _createInvoice)
        {
            #region KayitVarmiKontrolu
            if (allList.Count <= 0)
            {
                WriteLog(allList, "İlgili tarihlere ait aktarılacak banka hareketi bulunamadı.", LogoPostResult.NoDataFoundToInsert);
                if (Progress.MaxValue == 0)
                    Progress.MaxValue = 1;

                Progress.CurrentValue = Progress.MaxValue;
                Progress.NoDataFound = true;
                return;
            }
            #endregion

            #region Ulke Sehir bilgilerinin doldurulması
            foreach (var item in allList)
            {
                countryVM = countryVMList.FirstOrDefault(x => x.Code == item.CustCountry);
                cityVM = cityVMList.FirstOrDefault(x => x.Code == item.CustCity
                                                        && x.CountryNr == countryVM.CountryNr);
                townVM = townVMList.FirstOrDefault(x => x.Code == item.CustTown
                                                        && x.CityRef == cityVM.LogicalRef);

                if (countryVM != null)
                    item.CountryName = countryVM.Name;
                else
                    item.CountryName = "";

                if (cityVM != null)
                    item.CityName = cityVM.Name;
                else
                    item.CityName = "";

                if (townVM != null)
                    item.TownName = townVM.Name;
                else
                    item.TownName = "";
            }
            #endregion

            #region Listelerin Oluşturulması
            List<SourceTransVM> bankAccountTransactionList = new List<SourceTransVM>();
            bankAccountTransactionList.AddRange(allList.Where(x => x.TranAccType == 1
                                                                 && x.OldSlipRefNumber == 0
                                                                 && x.SlipDate >= approvalDate
                                                                 && x.TranTotal > 0
                                                                 && x.SlipRefNumber > 0
                                                                 && x.TranSerialNumber > 0));

            List<SourceTransVM> bankTransactionList = new List<SourceTransVM>();
            // List<SourceTransVM> unsortedBankTransactionList = new List<SourceTransVM>();
            bankTransactionList.AddRange(allList.Where(// MS: 2018-06-10 artık TranAcctype değerleri daha fazla x => x.TranAccType == 2
                                                        x => x.TranAccType != 1
                                                        && x.OldSlipRefNumber == 0
                                                        && x.SlipDate >= approvalDate
                                                        && x.TranTotal > 0
                                                        && x.SlipRefNumber > 0
                                                        && x.TranSerialNumber > 0));

            List<SourceTransVM> arpTransactionList = new List<SourceTransVM>();
            arpTransactionList.AddRange(allList.Where(x => x.OldSlipRefNumber != 0
                                                          && x.TranAccType == 2));
            //arpTransactionList = unsortedArpTransactionList.OrderBy(x => x.OldSlipRefNumber)
            //                  .ThenBy(x => x.SlipDate)
            //                  .ThenBy(x => x.SlipBranchNr)
            //                  .ThenBy(x => x.SlipRefNumber).ToList();
            #endregion

            #region Eski tarihli cari fişlerinin listesinin oluşturulması
            List<SourceTransVM> arpTransactionListWithOldDate = new List<SourceTransVM>();
            List<SourceTransVM> unsortedarpTransactionListWithOldDate = new List<SourceTransVM>();
            unsortedarpTransactionListWithOldDate.AddRange(allList.Where(x => x.OldSlipRefNumber != 0
                                                          && x.TranAccType == 2
                                                          && x.OldSlipDate != null));

            List<SourceTransVM> arpTransactionListWithOD = new List<SourceTransVM>();
            foreach (var item in unsortedarpTransactionListWithOldDate)
            {
                item.SlipDate = (DateTime)item.OldSlipDate;
                arpTransactionListWithOD.Add(item);
            }
            arpTransactionListWithOldDate = arpTransactionListWithOD.OrderBy(x => x.OldSlipRefNumber)
                              .ThenBy(x => x.SlipDate)
                              .ThenBy(x => x.SlipBranchNr)
                              .ThenBy(x => x.SlipRefNumber).ToList();
            #endregion

            WriteLog(new List<SourceTransVM>(), "Aktarım başladı.", LogoPostResult.NoDataFoundToInsert);
            #region Kontroller
            List<SourceTransVM> accountProblemList = new List<SourceTransVM>();
            accountProblemList.AddRange(allList.Where(x => x.TranAccType == 1
                                                                 && x.OldSlipRefNumber == 0
                                                                 && x.SlipDate >= approvalDate
                                                                 && x.TranTotal > 0
                                                                 && x.SlipRefNumber > 0
                                                                 && x.TranSerialNumber > 0
                                                                 && x.TranAccountCode == ""));
            WriteLog(accountProblemList, "TranAccountCode dolu olmalıdır.");



            List<SourceTransVM> dateProblemList = new List<SourceTransVM>();
            dateProblemList.AddRange(allList.Where(x => x.OldSlipRefNumber == 0
                                                    && x.SlipDate < approvalDate));

            WriteLog(dateProblemList, "Hareket tarihi onay tarihinden sonra olmalıdır.");

            List<SourceTransVM> amountProblemList = new List<SourceTransVM>();
            amountProblemList.AddRange(allList.Where(x => x.OldSlipRefNumber == 0
                                                    && x.TranTotal <= 0));
            WriteLog(amountProblemList, "Tutar sıfırdan büyük olmalıdır.");


            List<SourceTransVM> slipRefProblemList = new List<SourceTransVM>();
            slipRefProblemList.AddRange(allList.Where(x => x.SlipRefNumber <= 0));
            WriteLog(slipRefProblemList, "SlipRefNumber sıfırdan büyük olmalıdır.");

            List<SourceTransVM> tranSerialProblemList = new List<SourceTransVM>();
            tranSerialProblemList.AddRange(allList.Where(x => x.TranSerialNumber <= 0));
            WriteLog(tranSerialProblemList, "TranSerialNumber sıfırdan büyük olmalıdır.");
            #endregion

            #region Progress bar sayıyor görünsün
            for (int j = 1; j <= Progress.MaxValue - bankTransactionList.Count - arpTransactionList.Count - bankAccountTransactionList.Count; j++)//Hatalıların sayısı kadar arttırdık
            {
                Progress.CurrentValue = j;
            }
            #endregion

            #region Daha önce aktrılmış banka hareketleri kontrolü
            List<SourceTransVM> alreadyInsertedProblemList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(bankTransactionList, false, false); //Daha önce aktarılan kayıtları aldık

            WriteLog(alreadyInsertedProblemList, "Daha önce aktarılmış kayıt.");
            Progress.CurrentValue += alreadyInsertedProblemList.Count;

            bankTransactionList.RemoveAll(b => alreadyInsertedProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                       && i.SlipDate == b.SlipDate
                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                       && i.TranSerialNumber == b.TranSerialNumber));
            #endregion

            #region Daha önce aktarılmış cari fiş kontrolleri ve listeden çıkarılması
            //Kaydın kendi tarihiyle kontrol
            List<SourceTransVM> alreadyInsertedProblemListArp = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(arpTransactionList, false, true); //Daha önce aktarılan cari fiş kayıtları aldık
            WriteLog(alreadyInsertedProblemListArp, "Daha önce aktarılmış kayıt.");
            Progress.CurrentValue += alreadyInsertedProblemListArp.Count;

            arpTransactionList.RemoveAll(b => alreadyInsertedProblemListArp.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                       && i.SlipDate == b.SlipDate
                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                       //&& i.TranSerialNumber == b.TranSerialNumber
                                                                                       ));

            //Eski fişin tarihi ile kontrol
            List<SourceTransVM> alreadyInsertedProblemListArpOldDate = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(arpTransactionListWithOldDate, false, true); //Daha önce aktarılan eski tarihli cari fiş kayıtları aldık
            WriteLog(alreadyInsertedProblemListArpOldDate, "Daha önce aktarılmış kayıt.");
            Progress.CurrentValue += alreadyInsertedProblemListArpOldDate.Count;

            arpTransactionList.RemoveAll(b => alreadyInsertedProblemListArpOldDate.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                       && i.OriginalSlipDate == b.SlipDate
                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                       //&& i.TranSerialNumber == b.TranSerialNumber
                                                                                       ));

            #endregion
            #region Problemli kayıtların listeden çıkarılması
            bankTransactionList.RemoveAll(b => dateProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                       && i.SlipDate == b.SlipDate
                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                       && i.TranSerialNumber == b.TranSerialNumber));

            bankTransactionList.RemoveAll(b => amountProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                       && i.SlipDate == b.SlipDate
                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                       && i.TranSerialNumber == b.TranSerialNumber));

            bankTransactionList.RemoveAll(b => slipRefProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                       && i.TranSerialNumber == b.TranSerialNumber));

            bankTransactionList.RemoveAll(b => tranSerialProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                       && i.TranSerialNumber == b.TranSerialNumber));

            bankTransactionList.RemoveAll(b => accountProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                      ));
            bankAccountTransactionList.RemoveAll(b => accountProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                      ));

            arpTransactionList.RemoveAll(b => slipRefProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                       && i.TranSerialNumber == b.TranSerialNumber));

            arpTransactionList.RemoveAll(b => tranSerialProblemList.Exists(i => i.SlipBranchNr == b.SlipBranchNr
                                                                                                       && i.SlipDate == b.SlipDate
                                                                                                       && i.SlipRefNumber == b.SlipRefNumber
                                                                                                       && i.TranSerialNumber == b.TranSerialNumber));
            #endregion

            #region Banka satırlarına banka hesap numarasının yazılması
            foreach (var bankAccount in bankAccountTransactionList)
            {
                foreach (var bankTransaction in bankTransactionList.Where(bt => bt.SlipBranchNr == bankAccount.SlipBranchNr
                                                                          && bt.SlipDate == bankAccount.SlipDate
                                                                          && bt.SlipRefNumber == bankAccount.SlipRefNumber)
                                                                         )
                {
                    bankTransaction.BankAccountCode = bankAccount.TranAccountCode;
                }
            }

            for (int j = 1; j <= bankAccountTransactionList.Count; j++)//Hatalıların sayısı kadar arttırdık
            {
                Progress.CurrentValue++;
                System.Threading.Thread.Sleep(5);
            }
            #endregion

            List<SourceTransVM> sortedBankTransactionList = new List<SourceTransVM>();
            sortedBankTransactionList = bankTransactionList.OrderBy(x => x.OldSlipRefNumber)
            .ThenBy(x => x.SlipDate)
            .ThenBy(x => x.SlipType)
            .ThenBy(x => x.SlipBranchNr)
            .ThenBy(x => x.BankAccountCode)
            .ThenBy(x => x.SlipRefNumber).ToList();

            InsertBankVoucher(sortedBankTransactionList, _createInvoice);

            InsertArpVirement(arpTransactionList);
            WriteLog(new List<SourceTransVM>(), "Aktarım tamamlandı.", LogoPostResult.NoDataFoundToInsert);
            if (Progress.MaxValue == 0)
                Progress.MaxValue = 1;

            Progress.CurrentValue = Progress.MaxValue;
        }

        private void ProcessExpenseList(List<ExpenseVM> allList)
        {
            #region KayitVarmiKontrolu
            if (allList.Count <= 0)
            {
                WriteExpenseLog(allList, "İlgili tarihlere ait aktarılacak banka hareketi bulunamadı.", LogoPostResult.NoDataFoundToInsert);
                if (Progress.MaxValue == 0)
                    Progress.MaxValue = 1;

                Progress.CurrentValue = Progress.MaxValue;
                Progress.NoDataFound = true;
                return;
            }
            #endregion
            WriteExpenseLog(new List<ExpenseVM>(), "Aktarım başladı.", LogoPostResult.NoDataFoundToInsert);

            #region Kontroller

            List<ExpenseVM> accountProblemList = new List<ExpenseVM>();
            accountProblemList.AddRange(allList.Where(x => x.InvCustomerCode == ""));
            WriteExpenseLog(accountProblemList, "TranAccountCode dolu olmalıdır.");

            List<ExpenseVM> dateProblemList = new List<ExpenseVM>();
            dateProblemList.AddRange(allList.Where(x => x.InvDate < approvalDate));
            WriteExpenseLog(dateProblemList, "Hareket tarihi onay tarihinden sonra olmalıdır.");

            List<ExpenseVM> amountProblemList = new List<ExpenseVM>();
            amountProblemList.AddRange(allList.Where(x => x.LineTotal <= 0));
            WriteExpenseLog(amountProblemList, "Tutar sıfırdan büyük olmalıdır.");

            List<ExpenseVM> slipRefProblemList = new List<ExpenseVM>();
            slipRefProblemList.AddRange(allList.Where(x => x.LineRefNumber <= 0));
            WriteExpenseLog(slipRefProblemList, "TranRefNumber sıfırdan büyük olmalıdır.");

            #endregion

            #region Daha önce aktrılmış banka hareketleri kontrolü
            List<ExpenseVM> alreadyInsertedProblemList = new SourceTransDO().SelectInsertedExpenseList(allList); //Daha önce aktarılan kayıtları aldık
            WriteExpenseLog(alreadyInsertedProblemList, "Daha önce aktarılmış kayıt.");

            Progress.CurrentValue += alreadyInsertedProblemList.Count;

            allList.RemoveAll(b => alreadyInsertedProblemList.Exists(i => i.LineRefNumber == b.LineRefNumber));
            #endregion


            #region Problemli kayıtların listeden çıkarılması
            allList.RemoveAll(b => dateProblemList.Exists(i => i.LineRefNumber == b.LineRefNumber));

            allList.RemoveAll(b => amountProblemList.Exists(i => i.LineRefNumber == b.LineRefNumber));

            allList.RemoveAll(b => slipRefProblemList.Exists(i => i.LineRefNumber == b.LineRefNumber));

            allList.RemoveAll(b => accountProblemList.Exists(i => i.LineRefNumber == b.LineRefNumber));

            #endregion

            InsertExpenseInvoice(allList);
            WriteExpenseLog(new List<ExpenseVM>(), "Aktarım tamamlandı.", LogoPostResult.NoDataFoundToInsert);

            if (Progress.MaxValue == 0)
                Progress.MaxValue = 1;

            Progress.CurrentValue = Progress.MaxValue;
        }

        public void InsertBankVoucherWeb(DateTime _beginDate, DateTime _endDate)
        {
          
            if (unityApp.Connect()

                && unityApp.UserLogin(logoUserName, logoPassword)
                && unityApp.CompanyLogin(logoFirmNr))
            {
                var tokenClient = new RestClient(ConfigurationManager.AppSettings["tokenUrlForCredit"]);
                var tokenRequest = new RestRequest(Method.POST);
                tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
                tokenRequest.AddHeader("cache-control", "no-cache");
                tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                                + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
                IRestResponse response = tokenClient.Execute(tokenRequest);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.ErrorMessage);
                }

                RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

                token = deserial.Deserialize<Token>(response);

                var getRecordsClient = new RestClient(ConfigurationManager.AppSettings["dataUrlForCredit"]);
                var getRecordsRequest = new RestRequest(Method.POST);
                getRecordsRequest.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                getRecordsRequest.AddHeader("cache-control", "no-cache");
                getRecordsRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                getRecordsRequest.AddHeader("authorization", "Bearer " + token.access_token);
                getRecordsRequest.AddParameter("application/x-www-form-urlencoded", "action=Accounting%2FGetAccountingEntries"
                                                                                        + "&StartDate="
                                                                                        + _beginDate.Year.ToString() + "-"
                                                                                        + _beginDate.Month.ToString() + "-"
                                                                                        + _beginDate.Day.ToString()
                                                                                        + "&EndDate="
                                                                                        + _endDate.Year.ToString() + "-"
                                                                                        + _endDate.Month.ToString() + "-"
                                                                                        + _endDate.Day.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = getRecordsClient.Execute(getRecordsRequest);
                if (response2.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response2.ErrorMessage);
                }

                //        var responseList = deserial.Deserialize<List<SourceTrans>>(response2);
                var accountingResponseList = deserial.Deserialize<List<AccountingResponse>>(response2);

                foreach (AccountingResponse accountingResponse in accountingResponseList) // Elimize hep 1 tane kayıt gelecek.
                {
                    RestSharp.RestResponse response3 = new RestSharp.RestResponse();

                    response3.Content = accountingResponse.Message;
                    WriteWebResponseLog(response3.Content);
                    var responseList2 = deserial.Deserialize<List<SourceTrans>>(response3);
                    List<SourceTransVM> allList = new List<SourceTransVM>();
                    foreach (SourceTrans source in responseList2)
                    {
                        //source.TranDate = source.TranDate.AddHours(3);
                        //source.TranDate = source.TranDate.Date;
                        /*
                        if (source.TranDate != null)
                        {
                            DateTime dt = (DateTime)source.TranDate;
                            dt = dt.AddHours(3);
                            source.TranDate = dt.Date;
                        }
                        if (source.CancellationTranDate != null)
                        {
                            DateTime dt = (DateTime)source.CancellationTranDate;
                            dt = dt.AddHours(3);
                            source.CancellationTranDate = dt.Date;
                        }*/
                        allList.Add(SourceTranVMMapper.Convert(source));
                    }
                    Progress.MaxValue = allList.Count;
                    Progress.CurrentValue = 0;

                    ProcessList(allList, false);
                }
            }
            else //of if (unityApp.Connect() && unityApp.UserLogin(this.logoUserName, this.logoPassword) && unityApp.CompanyLogin(this.logoFirmNr)) 
            {
                if (unityApp.GetLastErrorString() != null)
                    logoErrorMessage = unityApp.GetLastErrorString().ToString();
            }

            if (unityApp.CompanyLoggedIn)
                unityApp.CompanyLogout();
            if (unityApp.LoggedIn)
                unityApp.UserLogout();
            if (unityApp.Connected)
                unityApp.Disconnect();
        }

        public void InsertBankCreditTransactions(DateTime _beginDate, DateTime _endDate)
        {

            if (unityApp.Connect()

                && unityApp.UserLogin(logoUserName, logoPassword)
                && unityApp.CompanyLogin(logoFirmNr))
            {
                
                var getRecordsClient = new RestClient(ConfigurationManager.AppSettings["dataUrlForCredit"]);
                var param = new RequestData();
                param.StartDate = _beginDate.Year.ToString() + "-" + _beginDate.Month.ToString() + "-" + _beginDate.Day.ToString();
                param.EndDate = _endDate.Year.ToString() + "-" + _endDate.Month.ToString() + "-" + _endDate.Day.ToString();


                var getRecordsRequest = new RestRequest();
                
                getRecordsRequest.AddHeader("content-type", "application/json");
                
                getRecordsRequest.AddHeader("secret-key", "eea982e3 - 8883 - 4450 - 9a10 - 05228413b2d5");
                
                getRecordsRequest.AddJsonBody(param);

                IRestResponse response2 = getRecordsClient.Get(getRecordsRequest);
                if (response2.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response2.ErrorMessage);
                }

                //        var responseList = deserial.Deserialize<List<SourceTrans>>(response2);
                var accountingResponseList = deserial.Deserialize<List<AccountingResponse>>(response2);

                foreach (AccountingResponse accountingResponse in accountingResponseList) // Elimize hep 1 tane kayıt gelecek.
                {
                    RestSharp.RestResponse response3 = new RestSharp.RestResponse();

                    response3.Content = accountingResponse.Message;
                    WriteWebResponseLog(response3.Content);
                    var responseList2 = deserial.Deserialize<List<SourceTrans>>(response3);
                    List<SourceTransVM> allList = new List<SourceTransVM>();
                    foreach (SourceTrans source in responseList2)
                    {
                        //source.TranDate = source.TranDate.AddHours(3);
                        //source.TranDate = source.TranDate.Date;
                        /*
                        if (source.TranDate != null)
                        {
                            DateTime dt = (DateTime)source.TranDate;
                            dt = dt.AddHours(3);
                            source.TranDate = dt.Date;
                        }
                        if (source.CancellationTranDate != null)
                        {
                            DateTime dt = (DateTime)source.CancellationTranDate;
                            dt = dt.AddHours(3);
                            source.CancellationTranDate = dt.Date;
                        }*/
                        allList.Add(SourceTranVMMapper.Convert(source));
                    }
                    Progress.MaxValue = allList.Count;
                    Progress.CurrentValue = 0;

                    ProcessList(allList, false);
                }
            }
            else //of if (unityApp.Connect() && unityApp.UserLogin(this.logoUserName, this.logoPassword) && unityApp.CompanyLogin(this.logoFirmNr)) 
            {
                if (unityApp.GetLastErrorString() != null)
                    logoErrorMessage = unityApp.GetLastErrorString().ToString();
            }

            if (unityApp.CompanyLoggedIn)
                unityApp.CompanyLogout();
            if (unityApp.LoggedIn)
                unityApp.UserLogout();
            if (unityApp.Connected)
                unityApp.Disconnect();
        }

        public void InsertExpenseWeb(DateTime _beginDate, DateTime _endDate)
        {
            if (unityApp.Connect()

                && unityApp.UserLogin(logoUserName, logoPassword)
                && unityApp.CompanyLogin(logoFirmNr))
            {
                var tokenClient = new RestClient(ConfigurationManager.AppSettings["tokenUrlForExpense"]);
                var tokenRequest = new RestRequest(Method.POST);
                tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
                tokenRequest.AddHeader("cache-control", "no-cache");
                tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                                + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
                IRestResponse response = tokenClient.Execute(tokenRequest);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.ErrorMessage);
                }

                RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

                token = deserial.Deserialize<Token>(response);

                var getRecordsClient = new RestClient(ConfigurationManager.AppSettings["dataUrlForExpense"]);
                var getRecordsRequest = new RestRequest(Method.POST);
                getRecordsRequest.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                getRecordsRequest.AddHeader("cache-control", "no-cache");
                getRecordsRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                getRecordsRequest.AddHeader("authorization", "Bearer " + token.access_token);
                getRecordsRequest.AddParameter("application/x-www-form-urlencoded", "action=GetExpenseTransactions"
                                                                                        + "&StartDate="
                                                                                        + _beginDate.Year.ToString() + "-"
                                                                                        + _beginDate.Month.ToString().PadLeft(2, '0') + "-"
                                                                                        + _beginDate.Day.ToString().PadLeft(2, '0')
                                                                                        + "&StopDate="
                                                                                        + _endDate.Year.ToString() + "-"
                                                                                        + _endDate.Month.ToString().PadLeft(2, '0') + "-"
                                                                                        + _endDate.Day.ToString().PadLeft(2, '0'), ParameterType.RequestBody);
                IRestResponse response2 = getRecordsClient.Execute(getRecordsRequest);
                if (response2.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response2.ErrorMessage);
                }

                //        var responseList = deserial.Deserialize<List<SourceTrans>>(response2);
                var accountingResponseList = deserial.Deserialize<List<AccountingResponse>>(response2);

                foreach (AccountingResponse accountingResponse in accountingResponseList) // Elimize hep 1 tane kayıt gelecek.
                {
                    RestSharp.RestResponse response3 = new RestSharp.RestResponse();

                    response3.Content = accountingResponse.Message;
                    WriteWebResponseLog(response3.Content);
                    var responseList2 = deserial.Deserialize<List<SourceExpense>>(response3);
                    List<ExpenseVM> allList = new List<ExpenseVM>();
                    foreach (SourceExpense source in responseList2)
                    {
                        allList.Add(SourceTranVMMapper.ConvertExpense(source));
                    }
                    Progress.MaxValue = allList.Count;
                    Progress.CurrentValue = 0;

                    ProcessExpenseList(allList);
                }
            }
            else //of if (unityApp.Connect() && unityApp.UserLogin(this.logoUserName, this.logoPassword) && unityApp.CompanyLogin(this.logoFirmNr)) 
            {
                if (unityApp.GetLastErrorString() != null)
                    logoErrorMessage = unityApp.GetLastErrorString().ToString();
            }

            if (unityApp.CompanyLoggedIn)
                unityApp.CompanyLogout();
            if (unityApp.LoggedIn)
                unityApp.UserLogout();
            if (unityApp.Connected)
                unityApp.Disconnect();
        }

        public void InsertBankVoucherWithInvoiceWeb(DateTime _beginDate, DateTime _endDate)
        {
            if (unityApp.Connect()

                && unityApp.UserLogin(logoUserName, logoPassword)
                && unityApp.CompanyLogin(logoFirmNr))
            {
                var tokenClient = new RestClient(ConfigurationManager.AppSettings["tokenUrlForInvoice"]);
                var tokenRequest = new RestRequest(Method.POST);
                tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
                tokenRequest.AddHeader("cache-control", "no-cache");
                tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                                + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
                IRestResponse response = tokenClient.Execute(tokenRequest);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.ErrorMessage);
                }

                RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

                token = deserial.Deserialize<Token>(response);

                var getRecordsClient = new RestClient(ConfigurationManager.AppSettings["dataUrlForInvoice"]);
                var getRecordsRequest = new RestRequest(Method.POST);
                // getRecordsRequest.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                getRecordsRequest.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                getRecordsRequest.AddHeader("cache-control", "no-cache");
                getRecordsRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                getRecordsRequest.AddHeader("authorization", "Bearer " + token.access_token);
                getRecordsRequest.AddParameter("application/x-www-form-urlencoded", "action=GetInvoiceBankTransactions"
                                                                                        + "&StartDate="
                                                                                        + _beginDate.Year.ToString() + "-"
                                                                                        + _beginDate.Month.ToString().PadLeft(2, '0') + "-"
                                                                                        + _beginDate.Day.ToString().PadLeft(2, '0')
                                                                                        + "&StopDate="
                                                                                        + _endDate.Year.ToString() + "-"
                                                                                        + _endDate.Month.ToString().PadLeft(2, '0') + "-"
                                                                                        + _endDate.Day.ToString().PadLeft(2, '0'), ParameterType.RequestBody);
                IRestResponse response2 = getRecordsClient.Execute(getRecordsRequest);
                if (response2.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response2.ErrorMessage);
                }

                //        var responseList = deserial.Deserialize<List<SourceTrans>>(response2);
                var accountingResponseList = deserial.Deserialize<List<AccountingResponse>>(response2);

                foreach (AccountingResponse accountingResponse in accountingResponseList) // Elimize hep 1 tane kayıt gelecek.
                {
                    RestSharp.RestResponse response3 = new RestSharp.RestResponse();

                    response3.Content = accountingResponse.Message;
                    WriteWebResponseLog(response3.Content);
                    var responseList2 = JsonConvert.DeserializeObject<List<SourceTrans>>(accountingResponse.Message);
                    //var responseList2 = deserial.Deserialize<List<SourceTrans>>(response3);
                    List<SourceTransVM> allList = new List<SourceTransVM>();
                    foreach (SourceTrans source in responseList2)
                    {
                        //source.TranDate = source.TranDate.AddHours(3);
                        source.TranDate = source.TranDate.Date;
                        /* if (source.CancellationTranDate != null)
                         {
                             DateTime dt = (DateTime)source.CancellationTranDate;
                             dt = dt.AddHours(3);
                             source.CancellationTranDate = dt.Date;
                         }*/
                        allList.Add(SourceTranVMMapper.Convert(source));
                    }
                    Progress.MaxValue = allList.Count;
                    Progress.CurrentValue = 0;

                    ProcessList(allList, true);

                }
            }
            else //of if (unityApp.Connect() && unityApp.UserLogin(this.logoUserName, this.logoPassword) && unityApp.CompanyLogin(this.logoFirmNr)) 
            {
                if (unityApp.GetLastErrorString() != null)
                    logoErrorMessage = unityApp.GetLastErrorString().ToString();
            }

            if (unityApp.CompanyLoggedIn)
                unityApp.CompanyLogout();
            if (unityApp.LoggedIn)
                unityApp.UserLogout();
            if (unityApp.Connected)
                unityApp.Disconnect();
        }

        public void ResetRecordsOnWeb(int _slipBranchNr, int _slipRefNumber, int _slipRefNumberEnd, DateTime _slipdate, bool _createInvoice)
        {
            List<SourceTransVM> list = new List<SourceTransVM>();

            for (int i = _slipRefNumber; i <= _slipRefNumberEnd; i++)
            {
                SourceTransVM st = new SourceTransVM();
                st.SlipBranchNr = _slipBranchNr;
                st.SlipRefNumber = i;
                st.SlipDate = _slipdate;
                st.OriginalSlipDate = _slipdate;
                st.TigerTranLogicalRef = 0;
                list.Add(st);
            }
            var tokenClient = _createInvoice ? new RestClient(ConfigurationManager.AppSettings["tokenUrlForInvoice"]) : new RestClient(ConfigurationManager.AppSettings["tokenUrlForCredit"]);
            var tokenRequest = new RestRequest(Method.POST);
            tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
            tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
            tokenRequest.AddHeader("cache-control", "no-cache");
            tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                            + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = tokenClient.Execute(tokenRequest);

            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            token = deserial.Deserialize<Token>(response);
            UpdateRecords2Web(list, _createInvoice);
        }

        public void ResetExpenseRecordsOnWeb(int _slipRefNumber, int _slipRefNumberEnd, DateTime _slipDate)
        {
            List<ExpenseVM> list = new List<ExpenseVM>();

            for (int i = _slipRefNumber; i <= _slipRefNumberEnd; i++)
            {
                ExpenseVM st = new ExpenseVM();
                st.LineRefNumber = i;
                st.InvDate = _slipDate;
                st.LogoInvoiceRef = 0;
                list.Add(st);
            }
            var tokenClient = new RestClient(ConfigurationManager.AppSettings["tokenUrlForExpense"]);
            var tokenRequest = new RestRequest(Method.POST);
            tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
            tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
            tokenRequest.AddHeader("cache-control", "no-cache");
            tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                            + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = tokenClient.Execute(tokenRequest);

            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            token = deserial.Deserialize<Token>(response);
            UpdateRecords2Web4Expense(list);
        }


        WebResponse UpdateRecords2Web(List<SourceTransVM> _list, bool _createInvoice)
        {

            if (_createInvoice)
                return UpdateRecords2Web4Invoice(_list);

            WebResponse webResult = new WebResponse();

            webResult.result = true;

            var client = new RestClient(ConfigurationManager.AppSettings["dataUrlForCredit"]);

            List<SourceTransVM> list = _list.GroupBy(x => new
            {
                x.SlipBranchNr,
                x.OriginalSlipDate,
                x.SlipRefNumber,
                x.TigerTranLogicalRef
            })
                                                                .Select(b => new SourceTransVM
                                                                {
                                                                    SlipBranchNr = b.Key.SlipBranchNr,
                                                                    OriginalSlipDate = b.Key.OriginalSlipDate,
                                                                    SlipRefNumber = b.Key.SlipRefNumber,
                                                                    TigerTranLogicalRef = b.Key.TigerTranLogicalRef
                                                                })
                                                                .ToList();

            foreach (var item in list)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Bearer " + token.access_token);
                request.AddParameter("application/x-www-form-urlencoded", "action=Accounting%2FUpdateTransactionDetail"
                                                                                + "&TranDate=" + item.OriginalSlipDate.Date.Year.ToString() + "-"
                                                                                + item.OriginalSlipDate.Date.Month.ToString() + "-"
                                                                                + item.OriginalSlipDate.Date.Day.ToString()
                                                                                 + "&TranBranchCode=" + item.SlipBranchNr.ToString() +
                                                                                 "&TranRefNum=" + item.SlipRefNumber +
                                                                                 "&TranLogicalRef=" + item.TigerTranLogicalRef.ToString(), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    webResult.resultText += response.ErrorMessage + "\n";
                    webResult.result = false;
                }
            }
            return webResult;

        }

        public WebResponse UpdateRecords2Web4Invoice(List<SourceTransVM> _list)
        {
            /*var tokenClient = new RestClient(ConfigurationManager.AppSettings["tokenUrl"]);
            var tokenRequest = new RestRequest(Method.POST);
            tokenRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
            tokenRequest.AddHeader("postman-token", "cafae71b-b19e-6e07-1a3d-c3a5580cdfb5");
            tokenRequest.AddHeader("cache-control", "no-cache");
            tokenRequest.AddParameter("application/x-www-form-urlencoded", "username=" + ConfigurationManager.AppSettings["tokenUserName"] + "&password="
                                                                            + ConfigurationManager.AppSettings["tokenPassword"] + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse responsetoken = tokenClient.Execute(tokenRequest);
            if (responsetoken.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApplicationException(responsetoken.ErrorMessage);
            }

            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            token = deserial.Deserialize<Token>(responsetoken);
            */

            WebResponse webResult = new WebResponse();
            webResult.result = true;

            var client = new RestClient(ConfigurationManager.AppSettings["dataUrlForInvoice"]);


            List<SourceTransVM> list = _list.GroupBy(x => new
            {
                x.SlipBranchNr,
                x.OriginalSlipDate,
                x.SlipRefNumber,
                x.TigerTranLogicalRef
            })
                                                                .Select(b => new SourceTransVM
                                                                {
                                                                    SlipBranchNr = b.Key.SlipBranchNr,
                                                                    OriginalSlipDate = b.Key.OriginalSlipDate,
                                                                    SlipRefNumber = b.Key.SlipRefNumber,
                                                                    TigerTranLogicalRef = b.Key.TigerTranLogicalRef
                                                                })
                                                                .ToList();

            foreach (var item in list)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Bearer " + token.access_token);
                request.AddParameter("application/x-www-form-urlencoded", "action=UpdateInvoiceBankTransactions"
                                                                                   + "&TranBranchCode=" + item.SlipBranchNr.ToString()
                                                                                   + "&TranDate=" + item.OriginalSlipDate.Date.Year.ToString() + "-"
                                                                                   + item.OriginalSlipDate.Date.Month.ToString() + "-"
                                                                                   + item.OriginalSlipDate.Date.Day.ToString()
                                                                                   + "&TranRefNum=" + item.SlipRefNumber
                                                                                   + "&TranLogicalRef=" + item.TigerTranLogicalRef.ToString()
                                                                                    , ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    webResult.resultText += response.ErrorMessage + "\n";
                    webResult.result = false;
                }


            }

            return webResult;

        }

        WebResponse UpdateRecords2Web4Expense(List<ExpenseVM> _list)
        {
            WebResponse webResult = new WebResponse();

            webResult.result = true;

            var client = new RestClient(ConfigurationManager.AppSettings["dataUrlForExpense"]);

            /*  List<ExpenseVM> list = _list.GroupBy(x => new
            {
                x.InvDate,
                x.LineRefNumber,
                x.LogoInvoiceRef
            })
                                                                .Select(b => new ExpenseVM
                                                                {
                                                                    InvDate = b.Key.InvDate,
                                                                    LineRefNumber = b.Key.LineRefNumber,
                                                                    LogoInvoiceRef = b.Key.LogoInvoiceRef
                                                                })
                                                                .ToList();
            */
            foreach (var item in _list)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("postman-token", "c452eaf3-50e9-ad71-881e-c1e96e21d72b");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Bearer " + token.access_token);
                request.AddParameter("application/x-www-form-urlencoded", "action=UpdateExpenseTransactions"
                                                                                   + "&TranDate=" + item.InvDate.Date.Year.ToString() + "-"
                                                                                   + item.InvDate.Date.Month.ToString() + "-"
                                                                                   + item.InvDate.Date.Day.ToString()
                                                                                   + "&TranRefNum=" + item.LineRefNumber
                                                                                   + "&TranLogicalRef=" + item.LogoInvoiceRef.ToString()
                                                                                    , ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    webResult.resultText += response.ErrorMessage + "\n";
                    webResult.result = false;
                }
            }
            return webResult;

        }

        private void InsertExpenseInvoice(List<ExpenseVM> _expenseVMList)
        {
            List<ExpenseVM> sortedExpenseList = new List<ExpenseVM>();
            sortedExpenseList = _expenseVMList.OrderBy(x => x.InvDocNumber)
            .ThenBy(x=> x.InvDate)
            .ThenBy(x => x.InvCustomerCode)
            .ThenBy(x => x.InvBranchNr)
            .ThenBy(x => x.InvPacketCode)
            .ThenBy(x => x.AuthCode)
            .ThenBy(x => x.AuxilCode)
            .ToList();

            UnityObjects.Data purchInvoice = unityApp.NewDataObject(DataObjectType.doPurchInvoice);
            Lines purchInvoiceLines = purchInvoice.DataFields.FieldByName("TRANSACTIONS").Lines;

            foreach (var line in sortedExpenseList)
            {
                count++;

                #region Eski fişi kaydedip yeni fiş oluşturma
                if (prevInvDocNumber != line.InvDocNumber || prevInvDate != line.InvDate || prevInvCustomerCode != line.InvCustomerCode
                    || prevInvBranchNr != line.InvBranchNr || prevInvPackedCode != line.InvPacketCode
                    || prevAuthCode != line.AuthCode || prevAuxilCode != line.AuxilCode
                    || count >= maxLineCount4Vouchers)
                {
                    if (!firstIteration)
                    {
                        purchInvoice.FillAccCodes();

                        if(purchInvoice.Post())
                        {
                            int invRef = int.Parse(purchInvoice.DataFields.DBFieldByName("LOGICALREF").Value.ToString());
                            string invCode = purchInvoice.DataFields.DBFieldByName("FICHENO").Value.ToString();
                            List<ExpenseVM> ProcessedExpenseList = new List<ExpenseVM>();
                            foreach (var item in InProcessExpenseList)
                            {
                                item.LogoInvoiceRef = invRef;
                                item.LogoInvoiceCode = invCode;
                                ProcessedExpenseList.Add(item);
                            }
                            
                            WebResponse webResponse = new WebResponse();

                            webResponse = UpdateRecords2Web4Expense(ProcessedExpenseList);
                            if (webResponse.result)
                            {
                                WriteExpenseLog(ProcessedExpenseList, "başarılı", LogoPostResult.LogoSuccess);
                            }

                            else
                            {
                                WriteExpenseLog(ProcessedExpenseList, webResponse.resultText, LogoPostResult.NotLogoResult);
                            }
                        }
                        else //of  if(purchInvoice.Post())
                        {
                            logoErrorMessage = "";

                            for (int i = 0; i < purchInvoice.ValidateErrors.Count; i++)
                            {
                                logoErrorMessage += purchInvoice.ValidateErrors[i].Error.ToString() + "\n";
                            }
                            WriteExpenseLog(InProcessExpenseList, logoErrorMessage, LogoPostResult.LogoHasError);
                        }
                    }//end of if (!firstIteration)

                    purchInvoice.New();
                    count = 1; 
                    Progress.CurrentValue += InProcessExpenseList.Count;
                    InProcessExpenseList.Clear();

                    purchInvoice.DataFields.FieldByName("TYPE").Value = 4;
                    purchInvoice.DataFields.FieldByName("NUMBER").Value = "~";
                    purchInvoice.DataFields.FieldByName("DATE").Value = line.InvDate;

                    Object tm = 0;
                    unityApp.PackTime(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, ref tm);
                    purchInvoice.DataFields.FieldByName("TIME").Value = tm.ToString();

                    purchInvoice.DataFields.FieldByName("ARP_CODE").Value = line.InvCustomerCode;
                    purchInvoice.DataFields.FieldByName("DOC_DATE").Value = line.InvDate;
                    purchInvoice.DataFields.FieldByName("EINVOICE").Value = 0;
                    purchInvoice.DataFields.FieldByName("NOTES1").Value = line.InvPacketCode;
                    purchInvoice.DataFields.FieldByName("DEPARTMENT").Value = line.InvBranchNr;

                    purchInvoice.DataFields.FieldByName("POST_FLAGS").Value = 247;
                    purchInvoice.DataFields.FieldByName("SOURCE_WH").Value = 0;
                    purchInvoice.DataFields.FieldByName("SOURCE_COST_GRP").Value = 0;
                    purchInvoice.DataFields.FieldByName("DIVISION").Value = 0;
                    purchInvoice.DataFields.FieldByName("FACTORY").Value = 0;

                    purchInvoice.DataFields.FieldByName("AUXIL_CODE").Value = line.AuxilCode;
                    purchInvoice.DataFields.FieldByName("AUTH_CODE").Value = line.AuthCode;

                    purchInvoice.DataFields.FieldByName("DOC_NUMBER").Value = line.InvDocNumber;

                } //end of if (prevInvDate != line.InvDate || prevInvCustomerCode != line.InvCustomerCode
                #endregion

                firstIteration = false;
                prevInvDate = line.InvDate;
                prevInvCustomerCode = line.InvCustomerCode;
                prevInvBranchNr = line.InvBranchNr;
                prevInvPackedCode = line.InvPacketCode;
                prevAuxilCode = line.AuxilCode;
                prevAuthCode = line.AuthCode;
                prevInvDocNumber = line.InvDocNumber;
                InProcessExpenseList.Add(line);

                #region Fatura satırlarının oluşturulması

                SrvCard srvCard = new BasicDataDO().SelectSrvCardByCode(line.LineServiceCode);

                purchInvoiceLines.AppendLine();
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("TYPE").Value = 4;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("MASTER_CODE").Value = line.LineServiceCode;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("QUANTITY").Value = 1;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("PRICE").Value = line.LineTotal;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("UNIT_CODE").Value = "ADET";
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("VAT_INCLUDED").Value = 1;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("VAT_RATE").Value = srvCard.VAT;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("DESCRIPTION").Value = line.LineDesc;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("OHP_CODE1").Value = line.LineExpCenterCode;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("OHP_CODE2").Value = line.LineExpCenterCode;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("CURR_PRICE").Value = line.LineCurrency;
                purchInvoiceLines[purchInvoiceLines.Count - 1].FieldByName("ORGLOGOID").Value = line.LineRefNumber.ToString();

                #endregion
            }//end of foreach (var line in sortedExpenseList)

            if(purchInvoice.DataFields.FieldByName("DATE").Value > new DateTime(2000, 1, 1)) //Varsa elimizdeki son faturayı kaydediyoruz.
            {
                purchInvoice.FillAccCodes();

                if (purchInvoice.Post())
                {
                    int invRef = int.Parse(purchInvoice.DataFields.DBFieldByName("LOGICALREF").Value.ToString());
                    string invCode = purchInvoice.DataFields.DBFieldByName("FICHENO").Value.ToString();
                    List<ExpenseVM> ProcessedExpenseList = new List<ExpenseVM>();
                    foreach (var item in InProcessExpenseList)
                    {
                        item.LogoInvoiceRef = invRef;
                        item.LogoInvoiceCode = invCode;
                        ProcessedExpenseList.Add(item);
                    }
                  
                    WebResponse webResponse = new WebResponse();

                    webResponse = UpdateRecords2Web4Expense(ProcessedExpenseList);
                    if (webResponse.result)
                    {
                        WriteExpenseLog(ProcessedExpenseList, "başarılı", LogoPostResult.LogoSuccess);
                    }

                    else
                    {
                        WriteExpenseLog(ProcessedExpenseList, webResponse.resultText, LogoPostResult.NotLogoResult);
                    }
                }
                else //of  if(purchInvoice.Post())
                {
                    logoErrorMessage = "";

                    for (int i = 0; i < purchInvoice.ValidateErrors.Count; i++)
                    {
                        logoErrorMessage += purchInvoice.ValidateErrors[i].Error.ToString() + "\n";
                    }
                    WriteExpenseLog(InProcessExpenseList, logoErrorMessage, LogoPostResult.LogoHasError);
                }
            }
        }

        private void InsertBankVoucher(List<SourceTransVM> _bankTransactionList, bool _createInvoice)
        {
            List<SourceTransVM> bankTransactionList = new List<SourceTransVM>();
            bankTransactionList = _bankTransactionList.OrderBy(x => x.OldSlipRefNumber)
            .ThenBy(x => x.SlipDate)
            .ThenBy(x => x.SlipType)
            .ThenBy(x => x.SlipBranchNr)
            .ThenBy(x => x.BankAccountCode)
            .ThenBy(x => x.SlipRefNumber)
            .ThenBy(x => x.TranAccType).ToList();


            UnityObjects.Data bankVoucher = unityApp.NewDataObject(DataObjectType.doBankVoucher);
            Lines bankTransactions = bankVoucher.DataFields.FieldByName("TRANSACTIONS").Lines;

            foreach (var line in bankTransactionList)
            {
                count++;

                if (prevSlipRefNumber != line.SlipRefNumber)
                {
                    proccessedSlipRefNumber++;

                    if (proccessedSlipRefNumber > slipRefNumbersLimit)
                        continue;

                }


                line.TranAccountCode = FindOrCreateArpCard(line);//MS: Cari hesap kodu belli kurallara göre belirleniyor.

                #region Eski fişi kaydedip yeni fiş oluşturma
                if (prevSlipRefNumber != line.SlipRefNumber
                    && (prevSlipDate != line.SlipDate || prevSlipType != line.SlipType
                        || prevSlipBranchNr != line.SlipBranchNr || prevBankAccountCode != line.BankAccountCode
                        || count >= maxLineCount4Vouchers))
                {
                    if (!firstIteration)
                    {
                        bankVoucher.DataFields.FieldByName("NOTES1").Value = "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString();

                        bankVoucher.FillAccCodes();

                        if (bankVoucher.Post())
                        {
                            WriteDebugLog("Banka fişini kaydettik." + "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString());

                            #region Kaydedilen referansları bul, webe yaz
                            WebResponse webResponse = new WebResponse();
                            List<SourceTransVM> processedList = new List<SourceTransVM>();
                            processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, false);
                            webResponse = UpdateRecords2Web(processedList, _createInvoice);
                            if (webResponse.result)
                            {
                                WriteLog(processedList, "başarılı", LogoPostResult.LogoSuccess);
                            }

                            else
                            {
                                WriteLog(processedList, webResponse.resultText, LogoPostResult.NotLogoResult);
                            }
                            #endregion

                            #region Fatura oluştur

                            if (_createInvoice)
                            {
                                invoiceLineList.Clear();
                                foreach (var item in InProcessList)
                                {
                                    if (item.TranAccType == 3)
                                        invoiceLineList.Add(item);
                                }


                                List<SourceTransVM> invoicedList = new List<SourceTransVM>();
                                invoicedList = InsertInvoice(invoiceLineList);

                                List<IncomingRefPayTransRef> payTransList = new List<IncomingRefPayTransRef>();

                                /*TODO: Sonra kapatılacak*/
                                // List<SourceTransVM> processedList = new List<SourceTransVM>();
                                processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, false);
                                /*TODO: Sonra kapatılacak*/

                                if (processedList.Count > 0)
                                {
                                    foreach (var item in processedList)
                                    {
                                        if (item.PayTransRef != 0 && item.TranAccType == 3 )
                                        {
                                            WriteDebugLog("Banka fişinin paytransını bulduk:" + item.PayTransRef.ToString());

                                            List<SourceTransVM> forClosingList = new List<SourceTransVM>();
                                            forClosingList.AddRange(invoicedList.Where(x => x.TranAccType != 1
                                                                                        && x.TranAccType != 2
                                                                                        && x.SlipBranchNr == item.SlipBranchNr
                                                                                        && x.SlipDate == item.SlipDate
                                                                                        && x.SlipRefNumber == item.SlipRefNumber));
                                            foreach (var element in forClosingList)
                                            {
                                                payTransList = new SourceTransDO().SelectPaytransByInvoiceRef(element);
                                                var payTrans = payTransList.Find(x => x.incomingRef == element.InvoiceRef);

                                                WriteDebugLog("Faturanın paytransrefini bulduk:" + payTrans.payTransRef.ToString());
                                                unityApp.DebtClose(item.PayTransRef, payTrans.payTransRef);
                                                WriteDebugLog("Ödeme eşleştirmesini yaptık.");
                                            }

                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        else //of if (bankVoucher.Post())
                        {
                            logoErrorMessage = "";

                            for (int i = 0; i < bankVoucher.ValidateErrors.Count; i++)
                            {
                                logoErrorMessage += bankVoucher.ValidateErrors[i].Error.ToString() + "\n";
                            }
                            WriteLog(InProcessList, logoErrorMessage, LogoPostResult.LogoHasError);
                        }
                    }

                    #region Yeni fişi oluştur sayaçları sıfırla
                    firstSlipRefNumber = line.SlipRefNumber;
                    count = 1;//Sayacı sıfırla ki bir dahaki 100 satır olduğunda fişi kaydedebilesin.

                    Progress.CurrentValue += InProcessList.Count;

                    InProcessList.Clear();
                    bankVoucher.New(); //Yeni fişi oluşturuyoruz
                    bankTransactions = bankVoucher.DataFields.FieldByName("TRANSACTIONS").Lines;

                    bankVoucher.DataFields.FieldByName("DATE").Value = line.SlipDate;
                    bankVoucher.DataFields.FieldByName("NUMBER").Value = "~";
                    bankVoucher.DataFields.FieldByName("DIVISION").Value = line.SlipBranchNr;
                    bankVoucher.DataFields.FieldByName("DEPARMENT").Value = 0;
                    bankVoucher.DataFields.FieldByName("TYPE").Value = line.SlipType;
                    bankVoucher.DataFields.FieldByName("CURRSEL_TOTALS").Value = 1;
                    bankVoucher.DataFields.FieldByName("GUID").Value = Guid.NewGuid();

                    #endregion
                }// end of if (!firstIteration && prevSlipRefNumber != line.SlipRefNumber 
                #endregion

                firstIteration = false;
                prevSlipDate = line.SlipDate;
                prevSlipType = line.SlipType;
                prevSlipBranchNr = line.SlipBranchNr;
                prevBankAccountCode = line.BankAccountCode;
                prevSlipRefNumber = line.SlipRefNumber;
                lastSlipRefNumber = line.SlipRefNumber;

                #region Satırların oluşturulması

                InProcessList.Add(line);

                bankTransactions.AppendLine();
                bankTransactions[bankTransactions.Count - 1].FieldByName("TYPE").Value = 1;
                bankTransactions[bankTransactions.Count - 1].FieldByName("ARP_CODE").Value = line.TranAccountCode;
                bankTransactions[bankTransactions.Count - 1].FieldByName("BANKACC_CODE").Value = line.BankAccountCode; //bankAccountSourceTrans.TranAccountCode; //
                bankTransactions[bankTransactions.Count - 1].FieldByName("DATE").Value = line.SlipDate;
                bankTransactions[bankTransactions.Count - 1].FieldByName("TRCODE").Value = line.SlipType;
                bankTransactions[bankTransactions.Count - 1].FieldByName("MODULENR").Value = 7;
                bankTransactions[bankTransactions.Count - 1].FieldByName("DOC_NUMBER").Value = line.TranDocumentNo;
                bankTransactions[bankTransactions.Count - 1].FieldByName("DESCRIPTION").Value = line.TranExp;
                if (line.SlipType == 3)
                {
                    bankTransactions[bankTransactions.Count - 1].FieldByName("DEBIT").Value = Math.Round(line.TranTotal, 2, MidpointRounding.AwayFromZero);
                    //bankTransactions[bankTransactions.Count - 1].FieldByName("SIGN").Value = 0;
                }

                if (line.SlipType == 4)
                {
                    bankTransactions[bankTransactions.Count - 1].FieldByName("CREDIT").Value = Math.Round(line.TranTotal, 2, MidpointRounding.AwayFromZero);
                    //bankTransactions[bankTransactions.Count - 1].FieldByName("SIGN").Value = 1;
                }

                bankTransactions[bankTransactions.Count - 1].FieldByName("AMOUNT").Value = Math.Round(line.TranTotal, 2, MidpointRounding.AwayFromZero);
                bankTransactions[bankTransactions.Count - 1].FieldByName("TC_XRATE").Value = 1;
                bankTransactions[bankTransactions.Count - 1].FieldByName("TC_AMOUNT").Value = Math.Round(line.TranTotal, 2, MidpointRounding.AwayFromZero);
                bankTransactions[bankTransactions.Count - 1].FieldByName("BANK_PROC_TYPE").Value = 2;
                bankTransactions[bankTransactions.Count - 1].FieldByName("DUE_DATE").Value = DateTime.Today;
                bankTransactions[bankTransactions.Count - 1].FieldByName("CUSTOM_DOC_NUMBER ").Value = line.TranSerialNumber.ToString();
                bankTransactions[bankTransactions.Count - 1].FieldByName("AFFECT_RISK").Value = 0;
                bankTransactions[bankTransactions.Count - 1].FieldByName("BN_CRDTYPE").Value = 1;
                bankTransactions[bankTransactions.Count - 1].FieldByName("DIVISION").Value = line.SlipBranchNr;
                bankTransactions[bankTransactions.Count - 1].FieldByName("GUID").Value = Guid.NewGuid();
                bankTransactions[bankTransactions.Count - 1].FieldByName("ORGLOGOID").Value = line.SlipRefNumber.ToString();
                #endregion
            } // end of foreach (var line in _bankTransactionList)
            #region Son banka fişinin kaydedilmesi
            if (bankVoucher.DataFields.FieldByName("DATE").Value != null && bankVoucher.DataFields.FieldByName("DATE").Value > new DateTime(2000, 1, 1))// (bankVoucher != null) //Varsa elimizdeki son fişi kaydediyoruz
            {
                bankVoucher.DataFields.FieldByName("NOTES1").Value = "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString();

                bankVoucher.FillAccCodes();

                if (bankVoucher.Post())
                {
                    WriteDebugLog("Banka fişini kaydettik." + "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString());

                    #region Kaydedilen referansları bul, webe yaz
                    WebResponse webResponse = new WebResponse();
                    List<SourceTransVM> processedList = new List<SourceTransVM>();
                    processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, false);
                    webResponse = UpdateRecords2Web(processedList, _createInvoice);
                    if (webResponse.result)
                        WriteLog(processedList, "başarılı", LogoPostResult.LogoSuccess);
                    else
                    {
                        WriteLog(processedList, webResponse.resultText, LogoPostResult.NotLogoResult);
                    }
                    #endregion

                    #region Fatura oluştur

                    if (_createInvoice)
                    {
                        invoiceLineList.Clear();
                        foreach (var item in InProcessList)
                        {
                            if (item.TranAccType == 3)
                                invoiceLineList.Add(item);
                        }


                        List<SourceTransVM> invoicedList = new List<SourceTransVM>();
                        invoicedList = InsertInvoice(invoiceLineList);

                        List<IncomingRefPayTransRef> payTransList = new List<IncomingRefPayTransRef>();

                        /*TODO: Sonra kapatılacak*/
                        //List<SourceTransVM> processedList = new List<SourceTransVM>();
                        processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, false);
                        /*TODO: Sonra kapatılacak*/

                        if (processedList.Count > 0)
                        {
                            foreach (var item in processedList)
                            {
                                if (item.PayTransRef != 0 && item.TranAccType == 3)
                                {
                                    WriteDebugLog("Banka fişinin paytransını bulduk:" + item.PayTransRef.ToString());

                                    List<SourceTransVM> forClosingList = new List<SourceTransVM>();
                                    forClosingList.AddRange(invoicedList.Where(x => x.TranAccType != 1
                                                                                && x.TranAccType != 2
                                                                                && x.SlipBranchNr == item.SlipBranchNr
                                                                                && x.SlipDate == item.SlipDate
                                                                                && x.SlipRefNumber == item.SlipRefNumber));
                                    foreach (var element in forClosingList)
                                    {
                                        payTransList = new SourceTransDO().SelectPaytransByInvoiceRef(element);
                                        var payTrans = payTransList.Find(x => x.incomingRef == element.InvoiceRef);



                                        WriteDebugLog("Faturanın paytransrefini bulduk:" + payTrans.payTransRef.ToString());
                                        unityApp.DebtClose(item.PayTransRef, payTrans.payTransRef);
                                        WriteDebugLog("Ödeme eşleştirmesini yaptık.");
                                    }

                                }
                            }
                        }

                        #endregion
                    }
                }

                else //of if (bankVoucher.Post())
                {
                    logoErrorMessage = "";

                    for (int i = 0; i < bankVoucher.ValidateErrors.Count; i++)
                    {
                        logoErrorMessage += bankVoucher.ValidateErrors[i].Error.ToString() + "\n";
                    }
                    WriteLog(InProcessList, logoErrorMessage, LogoPostResult.LogoHasError);
                }
                bankVoucher = null;
                Progress.CurrentValue += InProcessList.Count;
                InProcessList.Clear();
            }//end of if (bankVoucher.DataFields.FieldByName("DATE").Value != null && bankVoucher.DataFields.FieldByName("DATE").Value > new DateTime(2000, 1, 1))// (bankVoucher != null) //Varsa elimizdeki son fişi kaydediyoruz
            #endregion
        }

        private void InsertArpVirement(List<SourceTransVM> _arpTransactionList)
        {
            UnityObjects.Data arpVoucher = unityApp.NewDataObject(DataObjectType.doARAPVoucher);
            Lines arpTransactions = arpVoucher.DataFields.FieldByName("TRANSACTIONS").Lines;
            bool oldFound = false;
            count = 0;
            firstIteration = true;
            prevSlipDate = DateTime.MinValue;
            prevSlipType = 0;
            prevSlipBranchNr = 0;
            prevBankAccountCode = "";
            firstSlipRefNumber = 0;
            lastSlipRefNumber = 0;

            List<SourceTransVM> arpTransactionList = new List<SourceTransVM>();
            arpTransactionList = _arpTransactionList.OrderBy(x => x.OldSlipRefNumber)
                                .ThenBy(x => x.SlipDate)
                                .ThenBy(x => x.SlipBranchNr)
                                .ThenBy(x => x.SlipRefNumber).ToList();

            foreach (var line in arpTransactionList)
            {
                if (prevSlipRefNumber != line.SlipRefNumber)
                {
                    proccessedSlipRefNumber++;

                    if (proccessedSlipRefNumber > slipRefNumbersLimit)
                        continue;
                }
                #region Eski cari fişi kaydedip yeni fişi oluşturma
                if (prevSlipRefNumber != line.SlipRefNumber || prevSlipDate != line.SlipDate || prevSlipBranchNr != line.SlipBranchNr)
                {
                    if (!firstIteration && oldTranList.Count > 0 && InProcessList.Count > 0) //İlk iterasyonda elde fiş yok, heyecan yapma, kaydetmeye çalışma.
                    {

                        arpVoucher.DataFields.FieldByName("NOTES1").Value = "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString();
                        arpVoucher.FillAccCodes();
                        //    arpVoucher.ExportToXML("", @"D:\CariAkt1.xml");

                        double debitTotal = 0, creditTotal = 0;
                        debitTotal = oldTranList.Sum(x => x.TranTotal);
                        creditTotal = InProcessList.Sum(x => x.TranTotal);

                        if (debitTotal == creditTotal)
                        {
                            if (arpVoucher.Post())
                            {
                                WebResponse webResponse = new WebResponse();
                                List<SourceTransVM> processedList = new List<SourceTransVM>();
                                processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, true);

                                webResponse = UpdateRecords2Web(processedList, false);
                                if (webResponse.result)
                                    WriteLog(processedList, "başarılı", LogoPostResult.LogoSuccess);
                                else
                                {
                                    WriteLog(processedList, webResponse.resultText, LogoPostResult.NotLogoResult);
                                }
                            }

                            else
                            {
                                logoErrorMessage = "";
                                for (int i = 0; i < arpVoucher.ValidateErrors.Count; i++)
                                {
                                    logoErrorMessage += arpVoucher.ValidateErrors[i].Error.ToString() + "\n";
                                }
                                WriteLog(InProcessList, logoErrorMessage, LogoPostResult.LogoHasError);
                            }
                        }
                        else //of if(debitTotal == creditTotal)
                        {
                            WriteLog(InProcessList, "Borç alacak toplamı tutmuyor.", LogoPostResult.LogoHasError);
                        }


                        Progress.CurrentValue += InProcessList.Count;
                    }//end of if (!firstIteration) //İlk iterasyonda elde fiş yok, heyecan yapma, kaydetmeye çalışma.

                    firstIteration = false;
                    prevSlipDate = line.SlipDate;
                    prevSlipType = line.SlipType;
                    prevSlipBranchNr = line.SlipBranchNr;
                    prevSlipRefNumber = line.SlipRefNumber;
                    prevBankAccountCode = line.BankAccountCode;
                    firstSlipRefNumber = line.SlipRefNumber;

                    InProcessList.Clear();
                    oldTranList.Clear();
                    arpVoucher.New(); //Yeni fişi oluşturuyoruz
                    arpTransactions = arpVoucher.DataFields.FieldByName("TRANSACTIONS").Lines;

                    arpVoucher.DataFields.FieldByName("NUMBER").Value = "~";

                    arpVoucher.DataFields.FieldByName("TYPE").Value = 5;
                    arpVoucher.DataFields.FieldByName("DIVISION").Value = line.SlipBranchNr;
                    arpVoucher.DataFields.FieldByName("CURRSEL_TOTALS").Value = 1;

                    #region Borç satırlarının bulunup oluşturulması
                    List<SourceTransVM> tran = new List<SourceTransVM>();
                    tran.Add(line);

                    List<SourceTransVM> oldTrans = new List<SourceTransVM>();
                    oldTrans = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(tran, true, false); // line.OldSlipDate, line.OldSlipBranchNr, line.OldSlipRefNumber.ToString());

                    oldFound = oldTrans.Count > 0;

                    if (oldFound)
                    {
                        foreach (var oldTran in oldTrans)
                        {
                            oldTranList.Add(oldTran);
                            arpTransactions.AppendLine();
                            arpTransactions[arpTransactions.Count - 1].FieldByName("ARP_CODE").Value = oldTran.TranAccountCode;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("DESCRIPTION").Value = oldTran.TranExp;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("DEBIT").Value = oldTran.TranTotal;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("TC_AMOUNT").Value = oldTran.TranTotal;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("MONTH").Value = oldTran.SlipDate.Month;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("YEAR").Value = oldTran.SlipDate.Year;
                            arpTransactions[arpTransactions.Count - 1].FieldByName("DOC_NUMBER").Value = oldTran.TranDocumentNo;
                            oldSlipDate = oldTran.SlipDate;
                        }
                        arpVoucher.DataFields.FieldByName("DATE").Value = oldSlipDate < approvalDate ? line.SlipDate : oldSlipDate;
                    }
                    #endregion
                }//end of if (prevSlipRefNumber != line.SlipRefNumber || prevSlipDate != line.SlipDate || prevSlipBranchNr != line.SlipBranchNr)
                #endregion
                #region Cari virman alacak satırlarının oluşturulması
                if (oldFound)
                {
                    if (line.TranAccType != 1) //MS:2018-06-10 TranAccType değerleri fazlalaştı (line.TranAccType == 2)
                    {
                        line.SlipDate = oldSlipDate < approvalDate ? line.SlipDate : oldSlipDate;
                        InProcessList.Add(line);
                        arpTransactions.AppendLine();
                        arpTransactions[arpTransactions.Count - 1].FieldByName("ARP_CODE").Value = line.TranAccountCode;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("DESCRIPTION").Value = line.TranExp;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("CREDIT").Value = line.TranTotal;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("TC_AMOUNT").Value = line.TranTotal;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("MONTH").Value = line.SlipDate.Month;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("YEAR").Value = line.SlipDate.Year;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("DOC_NUMBER").Value = line.TranDocumentNo;
                        arpTransactions[arpTransactions.Count - 1].FieldByName("ORGLOGOID").Value = line.SlipRefNumber.ToString();
                    }

                }
                else // of if(oldFound)
                {
                    List<SourceTransVM> logList = new List<SourceTransVM>();
                    logList.Add(line);
                    WriteLog(logList, "Önceden aktarılmış hareket bulunamadı.");
                }
                lastSlipRefNumber = line.SlipRefNumber;
                #endregion
            }

            #region Son cari fişin kaydedilmesi
            if (arpVoucher.DataFields.FieldByName("DATE").Value != null && arpVoucher.DataFields.FieldByName("DATE").Value > new DateTime(2000, 1, 1))// (arpVoucher != null) //Varsa elimizdeki son fişi kaydediyoruz
            {

                arpVoucher.DataFields.FieldByName("NOTES1").Value = "Referans numaraları:" + firstSlipRefNumber.ToString() + " - " + lastSlipRefNumber.ToString();
                arpVoucher.FillAccCodes();
                //    arpVoucher.ExportToXML("", @"D:\CariAkt2.xml");
                double debitTotal = 0, creditTotal = 0;
                debitTotal = oldTranList.Sum(x => x.TranTotal);
                creditTotal = InProcessList.Sum(x => x.TranTotal);

                if (debitTotal == creditTotal)
                {
                    if (arpVoucher.Post())
                    {
                        WebResponse webResponse = new WebResponse();
                        List<SourceTransVM> processedList = new List<SourceTransVM>();
                        processedList = new SourceTransDO().SelectBySlipDateBranchNrSlipRef(InProcessList, false, true);
                        if (webResponse.result)
                            WriteLog(processedList, "başarılı", LogoPostResult.LogoSuccess);
                        else
                        {
                            WriteLog(processedList, webResponse.resultText, LogoPostResult.NotLogoResult);
                        }
                    }

                    else
                    {
                        logoErrorMessage = "";
                        for (int i = 0; i < arpVoucher.ValidateErrors.Count; i++)
                        {
                            logoErrorMessage += arpVoucher.ValidateErrors[i].Error.ToString() + "\n";
                        }
                        WriteLog(InProcessList, logoErrorMessage, LogoPostResult.LogoHasError);
                    }
                }
                else //of if(debitTotal == creditTotal)
                {
                    WriteLog(InProcessList, "Borç alacak toplamı tutmuyor.", LogoPostResult.LogoHasError);
                }

                Progress.CurrentValue += InProcessList.Count;
                arpVoucher = null;

                InProcessList.Clear();
                oldTranList.Clear();
            }
            #endregion
        }

        private List<SourceTransVM> InsertInvoice(List<SourceTransVM> _invoiceList)
        {
            List<SourceTransVM> result = new List<SourceTransVM>();
            UnityObjects.Data salesInvoice = unityApp.NewDataObject(DataObjectType.doSalesInvoice);
            Lines salesInvoiceLines = salesInvoice.DataFields.FieldByName("TRANSACTIONS").Lines;

            List<SourceTransVM> realList = new List<SourceTransVM>();
            realList.AddRange(_invoiceList.Where(x => x.TranAccType == 3));
            

            foreach (var line in realList)
            {
                List<SourceTransVM> LogList = new List<SourceTransVM>();
                LogList.Add(line);

              
                LogoClCard clCard = new LogoClCard();
                clCard = new CLCardDO().SelectByCustCode(line.TranAccountCode);

                SrvCard srvCard = new BasicDataDO().SelectSrvCardByCode(line.ServiceCode);

                salesInvoice.New();
                salesInvoiceLines = salesInvoice.DataFields.FieldByName("TRANSACTIONS").Lines;

                salesInvoice.DataFields.FieldByName("NUMBER").Value = "~";
                salesInvoice.DataFields.FieldByName("TYPE").Value = 7;//Perakende satış faturası
                salesInvoice.DataFields.FieldByName("DATE").Value = line.SlipDate;

                Object tm = 0;
                unityApp.PackTime(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, ref tm);
                salesInvoice.DataFields.FieldByName("TIME").Value = tm.ToString();

                salesInvoice.DataFields.FieldByName("ARP_CODE").Value = line.TranAccountCode;
                salesInvoice.DataFields.FieldByName("POST_FLAGS").Value = 247;
                //    salesInvoice.DataFields.FieldByName("VAT_RATE").Value = line.TranTotal == 0 ? 0 : Math.Round(VATLine.TranTotal * 100 / line.TranTotal, 1);

                salesInvoice.DataFields.FieldByName("DOC_DATE").Value = line.SlipDate;
                salesInvoice.DataFields.FieldByName("EINVOICE_TYPE").Value = clCard.EInvoiceTyp;
                salesInvoice.DataFields.FieldByName("PROFILE_ID").Value = clCard.ProfileId;
                //        salesInvoice.DataFields.FieldByName("EDURATION_TYPE").Value = 0;

                salesInvoice.DataFields.FieldByName("SOURCE_WH").Value = 0;
                salesInvoice.DataFields.FieldByName("SOURCE_COST_GRP").Value = 0;
                salesInvoice.DataFields.FieldByName("DIVISION").Value = 0;
                salesInvoice.DataFields.FieldByName("DEPARTMENT").Value = line.SlipBranchNr;
                salesInvoice.DataFields.FieldByName("FACTORY").Value = 0;
                salesInvoice.DataFields.FieldByName("EARCHIVEDETR_ISPERCURR").Value = clCard.IsPerCurr;

                if (clCard.AcceptEInvoice == 1) //EFatura mükellefi
                {
                    salesInvoice.DataFields.FieldByName("EINVOICE").Value = 1;
                }
                else //of if(clCard.AcceptEInvoice == 1) //EFatura mükellefi
                {
                    salesInvoice.DataFields.FieldByName("EINVOICE").Value = 2;

                    if (line.CustEMail != null && line.CustEMail != "")
                    {
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_SENDMOD").Value = 2;
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_EMAILADDR").Value = line.CustEMail;
                    }
                    else
                    {
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_SENDMOD").Value = 1;
                    }

                    if (line.CustType == 1) //Kurum
                    {
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_ISCOMP").Value = 1;
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_DEFINITION").Value = line.CustTitle;
                    }
                    else // of  if(line.CustType == 1) //Kurum 
                    {
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_ISCOMP").Value = 0;
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_NAME").Value = line.CustName;
                        salesInvoice.DataFields.FieldByName("EARCHIVEDETR_SURNAME").Value = line.CustSurname;
                    }

                    //salesInvoice.DataFields.FieldByName("EARCHIVEDETR_INTPAYMENTTYPE").Value = 4;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_INTPAYMENTDATE").Value = line.SlipDate;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_TAXNR").Value = line.CustTaxNr;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_TCKNO").Value = line.CustTCKN;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_NAME").Value = line.CustName;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_SURNAME").Value = line.CustSurname;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_CITYCODE").Value = line.CustCity;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_CITY").Value = line.CityName;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_COUNTRYCODE").Value = line.CustCountry;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_COUNTRY").Value = line.CountryName;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_TOWNCODE").Value = line.CustTown;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_TOWN").Value = line.TownName;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_ADDR1").Value = line.CustAddress1;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_ADDR2").Value = line.CustAddress2;
                    salesInvoice.DataFields.FieldByName("EARCHIVEDETR_TAXOFFICE").Value = line.CustTaxOffice;
                }

                salesInvoiceLines.AppendLine();
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("TYPE").Value = 4;                   // Satır tipi (0 : Malzeme)
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("SOURCEINDEX").Value = 0;            // Ambar numarası
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("SOURCECOSTGRP").Value = 0;          // Ambar maliyet grubu
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("MASTER_CODE").Value = line.ServiceCode; // Malzeme Kodu
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("QUANTITY").Value = 1;               // Miktar
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("PRICE").Value = line.TranTotal;               // Fiyat

                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("UNIT_CODE").Value = "ADET";         // Birim
                                                                                                                //    salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("VAT_RATE").Value = line.TranTotal == 0 ? 0 : Math.Round(VATLine.TranTotal * 100 / line.TranTotal, 1, MidpointRounding.AwayFromZero);             // Satır KDV Oranı
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("VAT_RATE").Value = srvCard.VAT;
                salesInvoiceLines[salesInvoiceLines.Count - 1].FieldByName("EDT_CURR").Value = 1;               // Dövizli para türü


                salesInvoice.FillAccCodes();

                if (!salesInvoice.Post())

                {
                    logoErrorMessage = "";

                    for (int i = 0; i < salesInvoice.ValidateErrors.Count; i++)
                    {
                        logoErrorMessage += salesInvoice.ValidateErrors[i].Error.ToString() + "\n";
                    }

                    WriteLog(LogList, logoErrorMessage, LogoPostResult.LogoHasError, LogSource.Invoice);

                }
                else
                {

                    line.InvoiceRef = int.Parse(salesInvoice.DataFields.DBFieldByName("LOGICALREF").Value.ToString());
                    line.InvoiceCode = salesInvoice.DataFields.DBFieldByName("FICHENO").Value.ToString();
                    result.Add(line);
                    WriteLog(LogList, "başarılı", LogoPostResult.LogoSuccess, LogSource.Invoice);

                    WriteDebugLog("Faturayı kaydettik, logicalref:" + line.InvoiceRef.ToString());
                }
            }
            return result;
        }

        private string FindOrCreateArpCard(SourceTransVM _sourceTransVM)
        {
            string arpCardCode = "";
            if (_sourceTransVM.TranAccType != 3 )
                arpCardCode = _sourceTransVM.TranAccountCode;
            else
            {
                LogoClCard logoClCard = new LogoClCard();
                logoClCard = new CLCardDO().SelectByCustTypeVKN(_sourceTransVM.CustType, _sourceTransVM.CustType == 1 ? _sourceTransVM.CustTaxNr : _sourceTransVM.CustTCKN);
                arpCardCode = logoClCard.Code == null ? "" : logoClCard.Code;
            }

            string PKN = "", GBN = "";
            if (arpCardCode == "" && (Convert.ToInt32(ConfigurationManager.AppSettings["controlDiyaLogo"]) == 0 ||
                                        CheckAcceptEInvoice(_sourceTransVM.CustType == 1 ? _sourceTransVM.CustTaxNr : _sourceTransVM.CustTCKN,
                                        out PKN,
                                        out GBN)
                                      )
                )
            {
                UnityObjects.Data arpCard = unityApp.NewDataObject(DataObjectType.doAccountsRP);
                arpCard.New();
                arpCard.DataFields.FieldByName("ACCOUNT_TYPE").Value = 3;
                arpCard.DataFields.FieldByName("CODE").Value = "~";
                arpCard.DataFields.FieldByName("TITLE").Value = _sourceTransVM.CustTitle;
                arpCard.DataFields.FieldByName("ADDRESS1").Value = _sourceTransVM.CustAddress1;
                arpCard.DataFields.FieldByName("ADDRESS2").Value = _sourceTransVM.CustAddress2;
                arpCard.DataFields.FieldByName("COUNTRY_CODE").Value = _sourceTransVM.CustCountry;
                arpCard.DataFields.FieldByName("CITY_CODE").Value = _sourceTransVM.CustCity;
                arpCard.DataFields.FieldByName("TOWN_CODE").Value = _sourceTransVM.CustTown;

                arpCard.DataFields.FieldByName("COUNTRY").Value = _sourceTransVM.CountryName;
                arpCard.DataFields.FieldByName("CITY").Value = _sourceTransVM.CityName;
                arpCard.DataFields.FieldByName("TOWN").Value = _sourceTransVM.TownName;

                arpCard.DataFields.FieldByName("E_MAIL").Value = _sourceTransVM.CustEMail;
                arpCard.DataFields.FieldByName("TAX_OFFICE_CODE").Value = _sourceTransVM.CustTaxOffice;
                arpCard.DataFields.FieldByName("POST_LABEL").Value = PKN;
                arpCard.DataFields.FieldByName("SENDER_LABEL").Value = GBN;
                arpCard.DataFields.FieldByName("ACCEPT_EINV").Value = 1;
                arpCard.DataFields.FieldByName("EINVOICE_TYPE").Value = 0;
                arpCard.DataFields.FieldByName("PROFILE_ID").Value = 1;

                if (_sourceTransVM.CustType == 1)
                {
                    arpCard.DataFields.FieldByName("PERSCOMPANY").Value = 0;
                    arpCard.DataFields.FieldByName("TAX_ID").Value = _sourceTransVM.CustTaxNr;

                }
                else //of if(_sourceTransVM.CustType == 1)
                {
                    arpCard.DataFields.FieldByName("PERSCOMPANY").Value = 1;
                    arpCard.DataFields.FieldByName("TCKNO").Value = _sourceTransVM.CustTCKN;
                    arpCard.DataFields.FieldByName("NAME").Value = _sourceTransVM.CustName;
                    arpCard.DataFields.FieldByName("SURNAME").Value = _sourceTransVM.CustSurname;
                }

                if (arpCard.Post())
                {
                    arpCardCode = arpCard.DataFields.DBFieldByName("CODE").Value.ToString();
                    List<SourceTransVM> aList = new List<SourceTransVM>();
                    aList.Add(_sourceTransVM);
                    WriteLog(aList, arpCardCode, LogoPostResult.LogoSuccess, LogSource.ArpCard);
                    WriteDebugLog("Cari kartı kaydettik, code:" + arpCardCode);
                }
                else //of if (arpCard.Post())
                {
                    logoErrorMessage = "";

                    for (int i = 0; i < arpCard.ValidateErrors.Count; i++)
                    {
                        logoErrorMessage += arpCard.ValidateErrors[i].Error.ToString() + "\n";
                    }
                    List<SourceTransVM> aList = new List<SourceTransVM>();
                    aList.Add(_sourceTransVM);
                    WriteLog(aList, logoErrorMessage, LogoPostResult.LogoHasError);
                }
            }

            if (arpCardCode == "")
                arpCardCode = _sourceTransVM.TranAccountCode;
            return arpCardCode;
        }

        private bool CheckAcceptEInvoice(string VKN, out string PKN, out string GBN)
        {
            bool isOk = false;

            try
            {
                tr.com.diyalogo.pb.eArchive eA = new tr.com.diyalogo.pb.eArchive();
                //DiyaLogoSVC.eArchiveSoapClient eA = new DiyaLogoSVC.eArchiveSoapClient();
                tr.com.diyalogo.pb.UserInfo userInfo;
                userInfo = eA.getValidateGIBUser(diyalogoUserName, diyalogoUserName, VKN);

                if (userInfo.PKN != "UNKNOWN")//|| uInfo.GBN != "UNKNOWN"
                    isOk = true;
                PKN = userInfo.PKN;
                GBN = userInfo.GBN;
            }
            catch (Exception ex)
            {
                throw new Exception("CheckAcceptEInvoice:" + VKN + ":" + ex.Message);
            }
            return isOk;
        }

        private void WriteLog(List<SourceTransVM> _inProcessList, string _result, LogoPostResult _logoPostResult = LogoPostResult.NotLogoResult, LogSource _sourceType = LogSource.BankVoucher)
        {

            int prevTigerTranLogicalRef = 0;

            var orderedList = _inProcessList.OrderBy(x => x.SlipBranchNr)
                          .ThenBy(x => x.OriginalSlipDate)
                          .ThenBy(x => x.SlipRefNumber)
                          .ThenBy(x => x.TranSerialNumber)
                          .ToList();

            using (StreamWriter outputFile = File.AppendText(logFileName))
            {
                string resultText;
                if (_inProcessList.Count <= 0)
                {
                    if (_logoPostResult == LogoPostResult.NoDataFoundToInsert)
                        outputFile.WriteLine(_result);
                    return;
                }
                if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                {
                    if (_sourceType == LogSource.BankVoucher)
                    {
                        outputFile.WriteLine("Fiş kaydedilirken şu hatalar alınmıştır:");
                        outputFile.WriteLine(_result);
                        outputFile.WriteLine("Yukarıdaki hatalar nedeniyle aşağıdaki hareketler kaydedilememiştir:");
                    }
                    if (_sourceType == LogSource.Invoice)
                    {
                        outputFile.WriteLine("Fatura kaydedilirken şu hatalar alınmıştır:");
                        outputFile.WriteLine(_result);
                        outputFile.WriteLine("Yukarıdaki hatalar nedeniyle aşağıdaki hareketler kaydedilememiştir:");
                    }

                }

                foreach (var item in orderedList)
                {
                    if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                       + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                       + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                       //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                       + " hareketinin durumu: Kaydedilemedi ";
                    }

                    else if (_logoPostResult == LogoPostResult.LogoSuccess && _sourceType == LogSource.BankVoucher)//Logo başarılı
                    {
                        if (prevTigerTranLogicalRef != item.TigerTranLogicalRef)
                        {
                            prevTigerTranLogicalRef = item.TigerTranLogicalRef;
                            resultText = item.ficheNo.ToString() + " numaralı ";
                            if (item.ficheType == 2)
                                resultText += "cari hesap virman fişi ";
                            else
                            {
                                if (item.SlipType == 3)
                                    resultText += "banka gelen havale fişi ";
                                if (item.SlipType == 4)
                                    resultText += "banka gönderilen havale fişi ";
                            }
                            resultText += "oluşturuldu.";
                            outputFile.WriteLine(resultText);
                        }

                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                       + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                       + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                       //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                       + " hareketinin durumu: Oluşturuldu ";
                    }
                    else if (_logoPostResult == LogoPostResult.LogoSuccess && _sourceType == LogSource.Invoice)//Logo başarılı
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                      + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                      + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                      //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                      + " hareketinin fatura durumu: "
                                      //+ item.InvoiceRef.ToString()
                                      //+ " referans numaralı "
                                      + item.InvoiceCode.ToString()
                                      + " numaralı fatura oluşturuldu.";
                    }
                    else if (_logoPostResult == LogoPostResult.LogoSuccess && _sourceType == LogSource.ArpCard)//Logo başarılı
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                      + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                      + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                      //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                      + " hareketinin durumu: "
                                      + _result
                                      + " kodlu cari hesap oluşturuldu.";
                    }
                    else
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                      + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                      + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                      //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                      + " hareketinin durumu: " + _result;
                    }
                    outputFile.WriteLine(resultText);
                }
            }
        }

        private void WriteExpenseLog(List<ExpenseVM> _inProcessList, string _result, LogoPostResult _logoPostResult = LogoPostResult.NotLogoResult)
        {
            var orderedList = _inProcessList
                .OrderBy(x => x.LogoInvoiceRef)
                .ThenBy(x => x.LineRefNumber)
                .ToList();
            string invoiceCode = "";
            using (StreamWriter outputFile = File.AppendText(logFileName))
            {
                string resultText = "";
                if (_inProcessList.Count <= 0)
                {
                    if (_logoPostResult == LogoPostResult.NoDataFoundToInsert)
                        outputFile.WriteLine(_result);
                    return;
                }
                if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                {
                    outputFile.WriteLine("Masraf kaydedilirken şu hatalar alınmıştır:");
                    outputFile.WriteLine(_result);
                    outputFile.WriteLine("Yukarıdaki hatalar nedeniyle aşağıdaki hareketler kaydedilememiştir:");
        
                }

                foreach (var item in orderedList)
                {
                    if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                    {
                        resultText = "LineRefNumber:" + item.LineRefNumber
                                      + " hareketinin durumu: Kaydedilemedi ";
                    }

                    else if (_logoPostResult == LogoPostResult.LogoSuccess )//Logo başarılı
                    {
                        if (invoiceCode != item.LogoInvoiceCode)
                        {
                            invoiceCode = item.LogoInvoiceCode;
                            resultText = invoiceCode + " numaralı fatura oluşturuldu.";
                            outputFile.WriteLine(resultText);
                        }
                        else
                        {
                            resultText = "";
                        }
                            
                        resultText = "LineRefNumber: " + item.LineRefNumber.ToString()
                                       + " hareketinin durumu:"
                                       + " Oluşturuldu.";
                    }
                    else
                    {
                        resultText = "LineRefNumber: " + item.LineRefNumber.ToString()
                                       + " hareketinin durumu: " 
                                       + _result;
                    }
                    outputFile.WriteLine(resultText);
                }
            }
        }

        private void WriteDebugLog(string _result)
        {
            Progress.ErrorMessage += "\n" +_result;
            if (ConfigurationManager.AppSettings["writeDebugLog"] != "1")
                return;

            using (StreamWriter outputFile = File.AppendText(debugFileName))
            {
                outputFile.WriteLine(_result);
            }
        }

        private void WriteWebResponseLog(string _result)
        {
            using (StreamWriter outputFile = File.AppendText(webLogFileName))
            {
                outputFile.WriteLine(_result);
            }
        }

        private string GetLogFileName(int _logFileType = 0) //0:Log, 1:Json, 2:debug
        {
            string folder = ConfigurationManager.AppSettings["logFileLocation"];
            string filter = "*.txt";
            string[] fileNames = Directory.GetFiles(folder, filter);

            string fileName = "";
            for (int i = 1; i < 100; i++)
            {
                if (_logFileType == 1)
                    fileName = folder + "WebdenGelenJson_" + DateTime.Today.Year.ToString() + '.'
                                                           + DateTime.Today.Month.ToString() + '.'
                                                           + DateTime.Today.Day.ToString()
                                                           + "_" + (i + 1000).ToString().Substring(1, 3) + ".txt";
                else if (_logFileType == 2)
                    fileName = folder + "Debug_" + DateTime.Today.Year.ToString() + '.'
                                                           + DateTime.Today.Month.ToString() + '.'
                                                           + DateTime.Today.Day.ToString()
                                                           + "_" + (i + 1000).ToString().Substring(1, 3) + ".txt";
                else
                    fileName = folder + "AktarimLogu_" + DateTime.Today.Year.ToString() + '.'
                                                          + DateTime.Today.Month.ToString() + '.'
                                                          + DateTime.Today.Day.ToString()
                                                          + "_" + (i + 1000).ToString().Substring(1, 3) + ".txt";
                if (!fileNames.Contains(fileName))
                    break;
            }

            return fileName;
        }
    }
    public class WebResponse
    {
        public bool result { get; set; }
        public string resultText { get; set; }

    }

    public class RequestData
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}