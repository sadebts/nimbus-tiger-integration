﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.DataOperation;
using TigerIntegration.Entity;
using TigerIntegration.Kernel;
using UnityObjects;

namespace TigerIntegration.Orchestration
{
    public class ArpCardOC
    {
        public string logoErrorMessage;

        public string FindOrCreateArpCard(SourceTransVM _sourceTransVM , UnityApplication unityApp)
        {
            
            string arpCardCode = "";
            if (_sourceTransVM.TranAccType != 3 && _sourceTransVM.TranAccType != 4)
                arpCardCode = _sourceTransVM.TranAccountCode;
            else
                arpCardCode = new CLCardDO().SelectByCustTypeVKN(_sourceTransVM.CustType, _sourceTransVM.CustType == 1 ? _sourceTransVM.CustTaxNr : _sourceTransVM.CustTCKN);

            if (arpCardCode == "" && Convert.ToInt32(ConfigurationManager.AppSettings["eInvoice"]) == 1)
            //TODO: eFatura mükellefi sorgulaması yapılacak.
            {
                UnityObjects.Data arpCard = unityApp.NewDataObject(DataObjectType.doAccountsRP);
                arpCard.New();
                arpCard.DataFields.FieldByName("ACCOUNT_TYPE").Value = 3;
                arpCard.DataFields.FieldByName("CODE").Value = "~";
                arpCard.DataFields.FieldByName("TITLE").Value = _sourceTransVM.CustTitle;
                arpCard.DataFields.FieldByName("ADDRESS1").Value = _sourceTransVM.CustAddress1;
                arpCard.DataFields.FieldByName("ADDRESS2").Value = _sourceTransVM.CustAddress2;
                arpCard.DataFields.FieldByName("COUNTRY_CODE").Value = _sourceTransVM.CustCountry;
                arpCard.DataFields.FieldByName("CITY_CODE").Value = _sourceTransVM.CustCity;
                arpCard.DataFields.FieldByName("TOWN_CODE").Value = _sourceTransVM.CustTown;
                arpCard.DataFields.FieldByName("E_MAIL").Value = _sourceTransVM.CustEMail;

                /*arpCard.DataFields.FieldByName("ACCEPT_EINV").Value = 1;
                arpCard.DataFields.FieldByName("EINVOICE_TYPE").Value = 1;
                

                /*TODO: Daha sonra açılacak.
                 * arpCard.DataFields.FieldByName("POST_LABEL").Value = 1;
                arpCard.DataFields.FieldByName("SENDER_LABEL").Value = 1;
                */

                if (_sourceTransVM.CustType == 1)
                {
                    arpCard.DataFields.FieldByName("PERSCOMPANY").Value = 0;
                    arpCard.DataFields.FieldByName("TAX_ID").Value = _sourceTransVM.CustTaxNr;

                }
                else //of if(_sourceTransVM.CustType == 1)
                {
                    arpCard.DataFields.FieldByName("PERSCOMPANY").Value = 1;
                    arpCard.DataFields.FieldByName("TCKNO").Value = _sourceTransVM.CustTCKN;
                    arpCard.DataFields.FieldByName("NAME").Value = _sourceTransVM.CustName;
                    arpCard.DataFields.FieldByName("SURNAME").Value = _sourceTransVM.CustSurname;
                }

                if (arpCard.Post())
                {
                    arpCardCode = arpCard.DataFields.DBFieldByName("CODE").Value.ToString();
                    List<SourceTransVM> aList = new List<SourceTransVM>();
                    aList.Add(_sourceTransVM);
                    new Logger().WriteLog(aList, "cari kart başarıyla kaydedildi", LogoPostResult.LogoSuccess);
                }
                else //of if (arpCard.Post())
                {
                    logoErrorMessage = "";

                    for (int i = 0; i < arpCard.ValidateErrors.Count; i++)
                    {
                        logoErrorMessage += arpCard.ValidateErrors[i].Error.ToString() + "\n";
                    }
                    List<SourceTransVM> aList = new List<SourceTransVM>();
                    aList.Add(_sourceTransVM);
                    new Logger().WriteLog(aList, logoErrorMessage, LogoPostResult.LogoHasError);
                }
            }

            if (arpCardCode == "")
                arpCardCode = _sourceTransVM.TranAccountCode;
            return arpCardCode;
        }
    }
}
