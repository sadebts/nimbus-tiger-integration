﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.DataOperation;
using TigerIntegration.Entity;

namespace TigerIntegration.Orchestration
{
    public class SysParameterOC
    {
        public SysParameterVM Select()
        {
            SysParameterVM sysParameterVM = new SysParameterVM();
            SysParameterDO sysParameterBS = new SysParameterDO();
            sysParameterVM = sysParameterBS.Select();
            return sysParameterVM;
        }
        public void Update(SysParameterVM _sysParameterVM)
        {
            SysParameterDO sysParameterBS = new SysParameterDO();
            sysParameterBS.Update(_sysParameterVM);
        }
    }
}
