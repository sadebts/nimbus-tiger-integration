﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.DataOperation.WebService;
using TigerIntegration.Entity;

namespace TigerIntegration.Kernel
{
    public static class SourceTranVMMapper
    {
        public static SourceTransVM Convert(SourceTrans source)
        {
            SourceTransVM target = new SourceTransVM();
            target.SlipDate = (DateTime)source.TranDate;
            target.SlipType = int.Parse(source.TranCode);
            target.SlipRefNumber = (int) source.TranRefNum;
            target.SlipBranchNr = (int)source.TranBranchCode;
            target.TranSign = source.DebitOrCredit;
            
            if (source.CancellationFlag == "Y")
            {
                target.OldSlipDate = DateTime.MinValue;
                target.OldSlipRefNumber = 0;
                target.OldSlipBranchNr = 0;
            }
            else
            {
                target.TranAccountCode = source.TranAccountCode;
                target.OldSlipDate = source.CancellationTranDate;
                target.OldSlipRefNumber = source.CancellationTranRefNum != null ? (int)source.CancellationTranRefNum : 0;
                target.OldSlipBranchNr = source.CancellationTranBranch != null ? (int)source.CancellationTranBranch : 0;
            }

            /* MS: 2018-06-10 Artık karşı taraf bu bilgiyi aynen gönderecek.
            if ((target.SlipType == 3 && target.TranSign == "D")
               || (target.SlipType == 4 && target.TranSign == "C"))
                target.TranAccType = 1;
            else
                target.TranAccType = 2;
            */
          //  target.TranAccType = (int) source.TranAccType;
            target.TranAccType = (int)source.TranLineType;

            target.TranAccountCode = source.TranAccountCode;
            target.TranTotal = (double) source.Amount;
            target.TranCurrency = source.Currency;
            target.TranDocumentNo = source.DocumentNumber == null ? "" : source.DocumentNumber.ToString();
            target.TranExp = source.Description;
            target.TranSerialNumber = (int) source.SerialNumber;
            target.OriginalSlipDate = target.SlipDate;

            target.CustType = source.CustType == "" ? (short) 0 : short.Parse(source.CustType);
            target.CustTaxOffice = source.CustTaxOffice;
            //target.CustSendType = (short) source.CustSendType;
            target.CustCountry = source.CustCountry;
            target.CustCity = source.CustCity;
            target.CustTown = source.CustTown;
            target.CustTaxNr = source.CustTaxNr;
            target.CustName = source.CustName;
            target.CustSurname = source.CustSurname;
            target.CustTCKN = source.CustTCKN;
            target.CustTitle = source.CustTitle;
            target.CustAddress1 = source.CustAddress1;
            target.CustAddress2 = source.CustAddress2;
            target.CustEMail = source.CustEMail;
            target.ServiceCode = source.ServiceCode;
            
            return target;
        }

        public static ExpenseVM ConvertExpense(SourceExpense source)
        {
            ExpenseVM target = new ExpenseVM();

            target.InvBranchNr = source.ExpBranchCode;
            target.InvCustomerCode = source.CustomerCode;
            target.InvDate = source.ExpenseDate;
            target.InvPacketCode = source.PacketCode;
            target.LineCurrency = source.Currency;
            target.LineDesc = source.Description;
            target.LineExpCenterCode = source.ExpCenterCode;
            target.LineRefNumber = source.TranRefNum;
            target.LineServiceCode = source.ServiceCode;
            target.LineTotal = source.Amount;
            target.AuthCode = source.AuthCode;
            target.AuxilCode = source.AuxilCode;
            target.InvDocNumber = source.OrderNo;         
            return target;
        }

    }
}
