﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TigerIntegration.Entity;

namespace TigerIntegration.Kernel
{
    public class Logger
    {
        public string logFileName = "";
        public string webLogFileName = "";

        public Logger()
        {
            logFileName = GetLogFileName();
            webLogFileName = GetLogFileName(true);
        }
        //
        //

        public void WriteLog(List<SourceTransVM> _inProcessList, string _result, LogoPostResult _logoPostResult = LogoPostResult.NotLogoResult, LogSource _sourceType = LogSource.Bankfiche)
        {

            int prevTigerTranLogicalRef = 0;

            var orderedList = _inProcessList.OrderBy(x => x.SlipBranchNr)
                          .ThenBy(x => x.OriginalSlipDate)
                          .ThenBy(x => x.SlipRefNumber)
                          .ThenBy(x => x.TranSerialNumber)
                          .ToList();

            using (StreamWriter outputFile = File.AppendText(logFileName))
            {
                string resultText;
                if (_inProcessList.Count <= 0)
                {
                    if (_logoPostResult == LogoPostResult.NoDataFoundToInsert)
                        outputFile.WriteLine(_result);
                    return;
                }
                if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                {
                    if (_sourceType == LogSource.Bankfiche)
                    {
                        outputFile.WriteLine("Fiş kaydedilirken şu hatalar alınmıştır:");
                        outputFile.WriteLine(_result);
                        outputFile.WriteLine("Yukarıdaki hatalar nedeniyle aşağıdaki hareketler kaydedilememiştir:");
                    }
                    if (_sourceType == LogSource.Invoice)
                    {
                        outputFile.WriteLine("Fatura kaydedilirken şu hatalar alınmıştır:");
                        outputFile.WriteLine(_result);
                        outputFile.WriteLine("Yukarıdaki hatalar nedeniyle aşağıdaki hareketler kaydedilememiştir:");
                    }

                }

                foreach (var item in orderedList)
                {
                    if (_logoPostResult == LogoPostResult.LogoHasError)//Logo hatası
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                       + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                       + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                       //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                       + " hareketinin durumu: Kaydedilemedi ";
                    }

                    else if (_logoPostResult == LogoPostResult.LogoSuccess && _sourceType == LogSource.Bankfiche)//Logo başarılı
                    {
                        if (prevTigerTranLogicalRef != item.TigerTranLogicalRef)
                        {
                            prevTigerTranLogicalRef = item.TigerTranLogicalRef;
                            resultText = item.ficheNo.ToString() + " numaralı ";
                            if (item.ficheType == 2)
                                resultText += "cari hesap virman fişi ";
                            else
                            {
                                if (item.SlipType == 3)
                                    resultText += "banka gelen havale fişi ";
                                if (item.SlipType == 4)
                                    resultText += "banka gönderilen havale fişi ";
                            }
                            resultText += "oluşturuldu.";
                            outputFile.WriteLine(resultText);
                        }

                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                       + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                       + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                       //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                       + " hareketinin durumu: Oluşturuldu ";
                    }
                    else if (_logoPostResult == LogoPostResult.LogoSuccess && _sourceType == LogSource.Invoice)//Logo başarılı
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                      + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                      + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                      //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                      + " hareketinin fatura durumu: Fatura oluşturuldu.";
                    }
                    else
                    {
                        resultText = "SlipBranchNr:" + item.SlipBranchNr.ToString()
                                      + " SlipRefNumber: " + item.SlipRefNumber.ToString()
                                      + " TranSerialNumber: " + item.TranSerialNumber.ToString()
                                      //   + " TigerTranLogicalref: " + item.TigerTranLogicalRef.ToString()
                                      + " hareketinin durumu: " + _result;
                    }
                    outputFile.WriteLine(resultText);
                }
            }
        }

        public void WriteWebResponseLog(string _result)
        {
            using (StreamWriter outputFile = File.AppendText(webLogFileName))
            {
                outputFile.WriteLine(_result);
            }
        }

        public string GetLogFileName(bool _isWebResponse = false)
        {
            string folder = ConfigurationManager.AppSettings["logFileLocation"];
            string filter = "*.txt";
            string[] fileNames = Directory.GetFiles(folder, filter);

            string fileName = "";
            for (int i = 1; i < 100; i++)
            {
                if (_isWebResponse)
                    fileName = folder + "WebdenGelenJson_" + DateTime.Today.ToShortDateString() + "_" + (i + 1000).ToString().Substring(1, 3) + ".txt";
                else
                    fileName = folder + "AktarimLogu_" + DateTime.Today.ToShortDateString() + "_" + (i + 1000).ToString().Substring(1, 3) + ".txt";
                if (!fileNames.Contains(fileName))
                    break;
            }

            return fileName;
        }
    }
}
