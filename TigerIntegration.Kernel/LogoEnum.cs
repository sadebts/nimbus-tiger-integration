﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Kernel
{
   public enum LogoPostResult
    {
        NotLogoResult,
        LogoHasError,
        LogoSuccess,
        NoDataFoundToInsert
    }
    public enum LogSource
    {
        BankVoucher,
        Invoice,
        ArpCard
    }
}
