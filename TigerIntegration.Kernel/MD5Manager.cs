﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace TigerIntegration.Kernel
{
    public class MD5Manager
    {

        public string EncryptData(string strKey, string
            strData)
        {
            string strResult; //Return Result

            DESCryptoServiceProvider descsp = new
                DESCryptoServiceProvider();

            descsp.Key = ASCIIEncoding.ASCII.GetBytes(strKey);
            descsp.IV = ASCIIEncoding.ASCII.GetBytes(strKey);

            ICryptoTransform desEncrypt = descsp.CreateEncryptor();

            MemoryStream mOut = new MemoryStream();
            CryptoStream encryptStream = new CryptoStream(mOut,
                desEncrypt, CryptoStreamMode.Write);

            byte[] rbData = UnicodeEncoding.Unicode.GetBytes(strData);
            try
            {
                encryptStream.Write(rbData, 0,
                    rbData.Length);
            }
            catch (Exception e)
            {
                // Catch it
            }

            // ADD: tell crypto stream you are done encrypting
            encryptStream.FlushFinalBlock();

            if (mOut.Length == 0)
                strResult = "";
            else
            {
                // ADD: use ToArray(). GetBuffer
                byte[] buff = mOut.ToArray();
                strResult = Convert.ToBase64String(buff, 0,
                    buff.Length);
            }

            try
            {
                encryptStream.Close();
            }
            catch (Exception e)
            {
                // Catch it
            }

            return strResult;
        }

        public string DecryptData(string strKey, string
            strData)
        {
            string strResult;

            DESCryptoServiceProvider descsp = new
                DESCryptoServiceProvider();

            descsp.Key = ASCIIEncoding.ASCII.GetBytes(strKey);
            descsp.IV = ASCIIEncoding.ASCII.GetBytes(strKey);

            ICryptoTransform desDecrypt = descsp.CreateDecryptor();

            MemoryStream mOut = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(mOut,
                desDecrypt, CryptoStreamMode.Write);
            char[] carray = strData.ToCharArray();
            byte[] rbData = Convert.FromBase64CharArray(carray,
                0, carray.Length);
            try
            {
                decryptStream.Write(rbData, 0,
                    rbData.Length);
            }
            catch (Exception e)
            {
                // Catch it
            }

            decryptStream.FlushFinalBlock();

            UnicodeEncoding aEnc = new UnicodeEncoding();
            strResult = aEnc.GetString(mOut.ToArray());

            try
            {
                decryptStream.Close();
            }
            catch (Exception e)
            {
                // Catch it
            }

            return strResult;
        }

    }
}
