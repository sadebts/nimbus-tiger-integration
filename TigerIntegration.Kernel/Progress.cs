﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerIntegration.Kernel
{
    public static class Progress
    {
        public static int MaxValue { get; set; }
        public static Boolean Error { get; set; }
        public static int CurrentValue { get; set; }
        public static bool NoDataFound { get; set; }
        public static string ErrorMessage { get; set; }
    }
}
