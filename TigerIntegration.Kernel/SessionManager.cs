﻿using TigerIntegration.DataOperation;
using TigerIntegration.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;



namespace TigerIntegration.Kernel
{
    public static class SessionManager
    {
        private static Dictionary<Guid, SysUserContextVM> activeSessions = new Dictionary<Guid, SysUserContextVM>();
        
        public static Guid Login(string _eMail, string _password, int _sysCustomerId)
        {
            Guid sessionId = Guid.NewGuid();
            MD5Manager md5 = new MD5Manager();
            
            if ( (_eMail == "ntiAdmin" && _password ==  ConfigurationManager.AppSettings["ntiAdminPass"])
                || (_eMail == "ntiUser" && _password == ConfigurationManager.AppSettings["ntiUserPass"])
                )
            {
                //activeSessions.Add(sessionId, new sysUserContextVM());
                return sessionId;
            }
            else //of if (sysUserContextVM.sysUserVM.eMail != null)
            {
                Exception e = new Exception("Login başarısız oldu");
                throw e;
            }

        }
        
        public static void Logout(Guid sessionId)
        {
            if(sessionId != null && activeSessions.ContainsKey(sessionId))
            {
                activeSessions.Remove(sessionId);
            }
        }
    }
}
